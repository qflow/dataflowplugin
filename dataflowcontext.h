#ifndef DATAFLOWCONTEXT_H
#define DATAFLOWCONTEXT_H

#include <coreplugin/icontext.h>

const char C_DATAFLOWDESIGNER[]       = "QmlDesigner::FormEditor";

class DataflowContext : public Core::IContext
{
public:
    DataflowContext(QWidget *widget);
    QString contextHelpId() const;
};

#endif // DATAFLOWCONTEXT_H
