#ifndef DATAFLOWDESIGNERWIDGET_H
#define DATAFLOWDESIGNERWIDGET_H

#include "zoomaction.h"
#include <QWidget>


class DataflowDesignerView;
class DataflowGraphicsView;
class QQmlEngine;

class DataflowDesignerWidgetPrivate;
class DataflowDesignerWidget : public QWidget
{
    Q_OBJECT
private slots:
    void resetNodeInstanceView();
    void setZoomLevel(double value);
    void onWheel(QWheelEvent * event);
public:
    DataflowDesignerWidget(DataflowDesignerView *view, QQmlEngine* engine);
    ~DataflowDesignerWidget();
    DataflowDesignerView* view() const;
    DataflowGraphicsView* graphicsView() const;
    QString contextHelpId() const;
protected:
private:
    const QScopedPointer<DataflowDesignerWidgetPrivate> d_ptr;
    Q_DECLARE_PRIVATE(DataflowDesignerWidget)

};

#endif // DATAFLOWDESIGNERWIDGET_H
