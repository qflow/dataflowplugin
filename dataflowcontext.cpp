#include "dataflowcontext.h"
#include "dataflowdesignerwidget.h"

DataflowContext::DataflowContext(QWidget *widget)
  : IContext(widget)
{
    setWidget(widget);
    setContext(Core::Context(C_DATAFLOWDESIGNER));
}

QString DataflowContext::contextHelpId() const
{
    return qobject_cast<DataflowDesignerWidget *>(m_widget)->contextHelpId();
}
