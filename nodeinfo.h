#ifndef NODEINFO_H
#define NODEINFO_H

#include <model.h>
#include <QQmlListProperty>
#include <QQuickItem>
#include <QSharedPointer>

class DataflowDesignerView;
class ListProperty;

using namespace QmlDesigner;

class NodeInfo;
typedef QSharedPointer<NodeInfo> NodeInfoPointer;
class NodeInfoPrivate;
class NodeInfo : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString nodeId READ nodeId NOTIFY nodeIdChanged)
    Q_PROPERTY(QString typeName READ typeName NOTIFY typeNameChanged)
    Q_PROPERTY(bool selected READ selected WRITE setSelected NOTIFY selectedChanged FINAL)
    Q_PROPERTY(QQuickItem* item READ item WRITE setItem NOTIFY itemChanged FINAL)
    Q_PROPERTY(QQmlListProperty<NodeInfo> children READ children NOTIFY childrenChanged)

public:
    explicit NodeInfo(QObject *parent = 0);
    ~NodeInfo();
    QString nodeId() const;
    void setNodeId(QString value);
    bool selected() const;
    void setSelected(bool value);
    void setInternalId(quint32 id);
    quint32 internalId() const;
    QString typeName() const;
    void setView(DataflowDesignerView* view);
    ModelNode modelNode() const;
    QQmlListProperty<NodeInfo> children();
    QQuickItem* item() const;
    void setItem(QQuickItem* item);
    NodeInfoPointer parentNodeInfo();
    void setContext(QQmlContext* context);
    QQmlContext* context() const;
    ListProperty* listProperty(QString name);
signals:
    void nodeIdChanged();
    void selectedChanged();
    void typeNameChanged();
    void itemChanged();
    void childrenChanged();
public slots:
    QVariant propertyValue(QString propertyName);
protected:
    const QScopedPointer<NodeInfoPrivate> d_ptr;
    NodeInfo(NodeInfoPrivate &dd, QObject *parent);
private:
    Q_DECLARE_PRIVATE(NodeInfo)
};

#endif // NODEINFO_H
