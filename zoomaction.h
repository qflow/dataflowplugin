#ifndef ZOOMACTION_H
#define ZOOMACTION_H

#include <QObject>
#include <QWidgetAction>
#include <QAbstractItemModel>

class ZoomAction : public QWidgetAction
{
    Q_OBJECT

public:
    ZoomAction(QObject *parent);

    double zoomLevel() const;

    void zoomIn();
    void zoomOut();

protected:
    QWidget *createWidget(QWidget *parent);
    void setZoomLevel(double zoomLevel);
signals:
    void zoomLevelChanged(double zoom);
    void indexChanged(int);
private slots:
    void emitZoomLevelChanged(int index);

private:
    QAbstractItemModel* m_comboBoxModel;
    double m_zoomLevel;
    int m_currentComboBoxIndex;
};

#endif // ZOOMACTION_H
