#ifndef TRIANGLE_H
#define TRIANGLE_H

#include <QQuickItem>

class Triangle : public QQuickItem
{
    Q_OBJECT
    Q_ENUMS(OrientationEnum)
    Q_PROPERTY(QColor color READ color WRITE setColor NOTIFY colorChanged)
    Q_PROPERTY(OrientationEnum orientation READ orientation WRITE setOrientation NOTIFY orientationChanged)

    QPolygonF getPolygon() const;

    QColor _color;
public:
    enum OrientationEnum{Left, Right, Bottom, Top};
private:
    OrientationEnum _orientation;
public:
    explicit Triangle(QQuickItem *parent = 0);
    bool contains(const QPointF & point) const;

    void setColor(QColor val);
    QColor color();

    void setOrientation(OrientationEnum value)
    {
        _orientation = value;
        emit orientationChanged();
    }
    OrientationEnum orientation() const
    {
        return _orientation;
    }

signals:
    void colorChanged();
    void angleChanged();
    void orientationChanged();
public slots:
protected:
    QSGNode* updatePaintNode(QSGNode *oldNode, UpdatePaintNodeData *data);
};

QML_DECLARE_TYPE(Triangle)

#endif // TRIANGLE_H
