#include "dataflowdesignerview.h"
#include "dataflowgraphscene.h"
#include "dataflowdesignerwidget.h"
#include "connectioninfo.h"
#include "librarynodeinfo.h"
#include "listproperty.h"
#include <metainfo.h>
#include <model.h>
#include <nodeproperty.h>
#include <QCoreApplication>
#include <bindingproperty.h>
#include <signalhandlerproperty.h>
#include <variantproperty.h>
#include <coreplugin/icore.h>
#include "dataflowcontext.h"
#include "dataflowgraphicsview.h"
#include <QQmlEngine>
#include <QQmlContext>
#include <QDir>

typedef QSharedPointer<QQmlComponent> QQmlComponentPointer;
class DataflowDesignerViewPrivate
{
public:
    QScopedPointer<QQmlEngine> _engine;
    QMap<qint32, NodeInfoPointer> _IdInfo;
    int _x;
    int _y;
    bool _center;
    QStringList _specificsPaths;
    QList<ModelNode> _connections;
    //QList<ModelNode> _directConnectionsTemp;
    QMap<QString, QQmlComponentPointer> _pathComponent;
    QString pluginPath();
    QString resourcePath();
    QString coordinatesFileName();
    QDir cacheDir();
    QPointer<DataflowDesignerWidget> _widget;
    void walk(ModelNode node);
    QString findSpecificsPath(ModelNode node);
    void processConnections();
    void serializeLayout();
    void layoutItems();
    DataflowDesignerViewPrivate(DataflowDesignerView* parent);
    NodeInfoPointer materializeNode(ModelNode node);
    NodeInfoPointer getNodeInfo(ModelNode node);
    DataflowGraphScene* scene() const;
    OutputPin* findOutputPin(QQuickItem *item, QString signalName);
    InputPin* findInputPin(QQuickItem *item, QString slotName);
    QSharedPointer<ConnectionInfo> _draggingConnection;
private:
    DataflowDesignerView* q_ptr;
    Q_DECLARE_PUBLIC(DataflowDesignerView)
};
DataflowDesignerViewPrivate::DataflowDesignerViewPrivate(DataflowDesignerView* parent) :
    _engine(new QQmlEngine()),
    _center(true),
    _widget(new DataflowDesignerWidget(parent, _engine.data())),
    q_ptr(parent)
{
    _specificsPaths << pluginPath();
    _specificsPaths << resourcePath();
    _engine->addImportPath("qrc:/dataflow/specifics");
    scene()->setView(parent);

    Core::ICore::addContextObject(new DataflowContext(_widget.data()));
}

DataflowDesignerView::DataflowDesignerView(QObject *parent) :
    AbstractView(parent), d_ptr(new DataflowDesignerViewPrivate(this))
{
}
void DataflowDesignerView::modelAttached(Model *model)
{
    Q_D(DataflowDesignerView);
    AbstractView::modelAttached(model);
    ModelNode rootNode = rootModelNode();
    d->_x = d->scene()->width()/2;
    d->_y = d->scene()->height()/2;
    d->walk(rootNode);
    d->layoutItems();
    d->processConnections();
    //d->_widget.data()->graphicsView()->centerOn(d->_x, d->_y);
}
NodeInfoPointer DataflowDesignerViewPrivate::materializeNode(ModelNode node)
{
    Q_Q(DataflowDesignerView);
    NodeInfoPointer info(getNodeInfo(node));
    info->setView(q);
    QObject::connect(info.data(), SIGNAL(selectedChanged()), q, SLOT(onSelectedChanged()));
    _IdInfo[node.internalId()] = info;
    info->setInternalId(node.internalId());

    QQmlComponentPointer component;
    QString specificsPath = findSpecificsPath(node);
    if(_pathComponent.contains(specificsPath))
    {
        component = _pathComponent[specificsPath];
    }
    else
    {
        if(specificsPath.startsWith(":")) specificsPath = "qrc" + specificsPath;
        QUrl url(specificsPath);
        component.reset(new QQmlComponent(_engine.data(), url));
        _pathComponent[node.type()] = component;
    }
    QQmlContext* context = new QQmlContext(_engine.data());
    context->setContextProperty("node", info.data());
    info->setContext(context);
    QObject *obj = component->create(context);
    QQuickItem *item = qobject_cast<QQuickItem*>(obj);
    qDebug() << component->errorString();
    info->setItem(item);
    info->setNodeId(node.id());
    scene()->addItem(item);

    return info;
}
NodeInfoPointer DataflowDesignerViewPrivate::getNodeInfo(ModelNode node)
{
    if(node.metaInfo().isSubclassOf("VideoPlugin.MediaConnection", 1, 0))
    {
        QSharedPointer<ConnectionInfo> connectionInfo(new ConnectionInfo());
        BindingProperty sourceProp = node.property("source").toBindingProperty();
        ModelNode sourceNode = sourceProp.resolveToModelNode();
        BindingProperty destinationProp = node.property("destination").toBindingProperty();
        ModelNode destinationNode = destinationProp.resolveToModelNode();

        NodeInfoPointer sourceInfo = _IdInfo[sourceNode.internalId()];
        NodeInfoPointer destinationInfo = _IdInfo[destinationNode.internalId()];

        OutputPin* outputPin = findOutputPin(sourceInfo->item(), "to(Frame)");
        if(!outputPin) outputPin = findOutputPin(sourceInfo->item(), "to(Packet)");
        InputPin* inputPin = findInputPin(destinationInfo->item(), "from(Frame)");
        if(!inputPin) inputPin = findInputPin(destinationInfo->item(), "from(Packet)");

        connectionInfo->setSignalPin(outputPin);
        connectionInfo->setSlotPin(inputPin);
        return connectionInfo;
    }
    if(node.metaInfo().isSubclassOf("GeneralPlugin.DirectConnection", 1, 0))
    {
        QSharedPointer<ConnectionInfo> connectionInfo(new ConnectionInfo());
        BindingProperty sourceProp = node.property("source").toBindingProperty();
        ModelNode sourceNode = sourceProp.resolveToModelNode();
        BindingProperty destinationProp = node.property("destination").toBindingProperty();
        ModelNode destinationNode = destinationProp.resolveToModelNode();
        VariantProperty signalProp = node.property("signal").toVariantProperty();
        QString signal = signalProp.value().toString();
        VariantProperty slotProp = node.property("slot").toVariantProperty();
        QString slot = slotProp.value().toString();

        NodeInfoPointer sourceInfo = _IdInfo[sourceNode.internalId()];
        NodeInfoPointer destinationInfo = _IdInfo[destinationNode.internalId()];

        InputPin* inputPin = findInputPin(destinationInfo->item(), slot);
        OutputPin* outputPin = findOutputPin(sourceInfo->item(), signal);


        connectionInfo->setSignalPin(outputPin);
        connectionInfo->setSlotPin(inputPin);
        return connectionInfo;
    }
    else
    {
        QSharedPointer<LibraryNodeInfo> libraryInfo(new LibraryNodeInfo());
        return libraryInfo;
    }
    return QSharedPointer<NodeInfo>();
}
NodeInfoPointer DataflowDesignerView::nodeInfo(quint32 internalId)
{
    Q_D(DataflowDesignerView);
    return d->_IdInfo[internalId];
}
QQmlEngine* DataflowDesignerView::engine() const
{
    Q_D(const DataflowDesignerView);
    return d->_engine.data();
}
QString DataflowDesignerViewPrivate::pluginPath()
{
    QFileInfo info(QCoreApplication::applicationDirPath() + "/../lib/qtcreator/plugins/");
    return info.absoluteFilePath();
}
QString DataflowDesignerViewPrivate::resourcePath()
{
    return ":/dataflow/specifics/";
}
QString DataflowDesignerViewPrivate::coordinatesFileName()
{
    Q_Q(DataflowDesignerView);
    QUrl fileUrl = q->model()->fileUrl();
    QString fileName = fileUrl.url(QUrl::FullyEncoded);
    fileName = QUrl::toPercentEncoding(fileName) + ".dat";
    return fileName;
}
QDir DataflowDesignerViewPrivate::cacheDir()
{
    QDir dir(pluginPath() + "/cache/");
    if(!dir.exists()) dir.mkdir(dir.absolutePath());
    return dir;
}

DataflowDesignerView::~DataflowDesignerView()
{
}

bool DataflowDesignerView::hasWidget() const
{
    return true;
}
WidgetInfo DataflowDesignerView::widgetInfo()
{
    Q_D(DataflowDesignerView);
    WidgetInfo info = AbstractView::createWidgetInfo(d->_widget.data(), 0, "DataflowDesigner",
                                   WidgetInfo::CentralPane, 1, "Dataflow Designer");
    return info;
}
DataflowGraphScene* DataflowDesignerViewPrivate::scene() const
{
    return _widget.data()->graphicsView()->scene();
}


OutputPin* DataflowDesignerViewPrivate::findOutputPin(QQuickItem *item, QString signalName)
{
    OutputPin* outputPin = dynamic_cast<OutputPin*>(item);
    if(outputPin)
    {
        QString sigName = outputPin->signalName();
        if(sigName == signalName) return outputPin;
    }
    foreach (QQuickItem* child, item->childItems())
    {
        QQuickItem* declItem = dynamic_cast<QQuickItem*>(child);
        if(declItem)
        {
            OutputPin* childOutputPin = findOutputPin(declItem, signalName);
            if(childOutputPin) return childOutputPin;
        }
    }
    return NULL;
}
InputPin* DataflowDesignerViewPrivate::findInputPin(QQuickItem *item, QString slotName)
{
    InputPin* inputPin = dynamic_cast<InputPin*>(item);
    if(inputPin && inputPin->slotName() == slotName) return inputPin;
    foreach (QQuickItem* child, item->childItems())
    {
        QQuickItem* declItem = dynamic_cast<QQuickItem*>(child);
        if(declItem)
        {
            InputPin* childInputPin = findInputPin(declItem, slotName);
            if(childInputPin) return childInputPin;
        }
    }
    return NULL;
}
void DataflowDesignerView::nodeCreated(const ModelNode &createdNode)
{
    Q_D(DataflowDesignerView);
    NodeInfoPointer info = d->materializeNode(createdNode);
    NodeInfoPointer parent = info->parentNodeInfo();
    if(parent) emit parent->childrenChanged();
}
void DataflowDesignerViewPrivate::walk(ModelNode node)
{
    foreach (ModelNode subNode, node.allSubModelNodes()) {
        walk(subNode);
    }
    QString typeName = node.metaInfo().typeName();
    if(typeName == "QtQuick.Connections")
    {
        _connections.append(node);
        return;
    }

    NodeInfoPointer newInfo = materializeNode(node);
    //emit newInfo->childrenChanged();
}
QString DataflowDesignerViewPrivate::findSpecificsPath(ModelNode node)
{
    QStringList paths = _specificsPaths;
    paths.push_front(node.metaInfo().importDirectoryPath() + "/specifics");
    foreach (QString path, paths) {
        QDir dir(path);
        QList<NodeMetaInfo> superClasses =  node.metaInfo().superClasses();
        foreach(NodeMetaInfo info, superClasses)
        {
            QString typeName = info.typeName().split('.').last();
            QString specFilename = QString("%1Specifics.qml").arg(typeName);
            if(dir.entryList().contains(specFilename))
            {
                QString absPath = dir.absoluteFilePath(specFilename);
                return absPath;
            }
        }
    }
    return resourcePath() + "QtObjectSpecifics.qml";
}
void DataflowDesignerViewPrivate::processConnections()
{
    Q_Q(DataflowDesignerView);
    foreach (ModelNode node, _connections)
    {
        BindingProperty prop = node.property("target").toBindingProperty();
        ModelNode targetNode = prop.resolveToModelNode();
        NodeInfoPointer targetInfo = _IdInfo[targetNode.internalId()];

        foreach(AbstractProperty property, node.properties())
        {
            if(property.isSignalHandlerProperty())
            {
                SignalHandlerProperty signalHandler = property.toSignalHandlerProperty();
                OutputPin* outputPin = findOutputPin(targetInfo->item(), signalHandler.name());
                QStringList components = signalHandler.source().split('.');
                QString destinationName = components[0];
                QString slotName = components[1];
                ModelNode destinationNode = q->modelNodeForId(destinationName);
                NodeInfoPointer destinationInfo = _IdInfo[destinationNode.internalId()];
                InputPin* inputPin = findInputPin(destinationInfo->item(), slotName);
                ConnectionInfo* connectionInfo = new ConnectionInfo();
                connectionInfo->setSignalPin(outputPin);
                connectionInfo->setSlotPin(inputPin);
                QUrl url("qrc:/dataflow/specifics/StandardConnection.qml");
                QQmlComponent* component = new QQmlComponent(_engine.data(), url);
                QQmlContext* context = new QQmlContext(_engine.data());
                context->setContextProperty("connection", connectionInfo);
                QObject *obj = component->create(context);
                QQuickItem *item = qobject_cast<QQuickItem*>(obj);
                scene()->addItem(item);

            }
        }
    }
}

void DataflowDesignerView::modelAboutToBeDetached(Model *model)
{
    Q_D(DataflowDesignerView);
    d->serializeLayout();
    d->_x = d->_y = 0;
    AbstractView::modelAboutToBeDetached(model);
}
void DataflowDesignerViewPrivate::serializeLayout()
{
    QString fileName = cacheDir().absoluteFilePath(coordinatesFileName());
    QFile file(fileName);
    QMap<quint32, QPoint> coordinates;
    foreach (quint32 internalId, _IdInfo.keys()) {
        NodeInfoPointer info = _IdInfo[internalId];
        QSharedPointer<ConnectionInfo> conInfo = info.objectCast<ConnectionInfo>();
        if(!conInfo)
        {
            QPoint point = info->item()->position().toPoint();
            coordinates[internalId] = point;
        }
        else
        {
        }
    }
    file.open(QIODevice::WriteOnly);
    QDataStream out(&file);
    out << coordinates;
    file.close();
}
void DataflowDesignerViewPrivate::layoutItems()
{
    QString fileName = cacheDir().absoluteFilePath(coordinatesFileName());
    QFile file(fileName);
    QMap<quint32, QPoint> coordinates;
    if(file.open(QIODevice::ReadOnly))
    {
        QDataStream in(&file);
        in >> coordinates;
        file.close();
    }
    foreach (NodeInfoPointer info, _IdInfo.values()) {
        if(info.objectCast<ConnectionInfo>())
        {
            continue;
        }
        if(coordinates.contains(info->internalId()))
        {
            info->item()->setPosition(coordinates[info->internalId()]);
        }
        else
        {

        }
    }
    foreach (NodeInfoPointer info, _IdInfo.values()) {
        NodeInfoPointer parentNodeInfo = info->parentNodeInfo();
        if(!parentNodeInfo) continue;
        NodeAbstractProperty parentProperty = info->modelNode().parentProperty();
        foreach(QQuickItem* item, DataflowDesignerView::getAllDescendands(parentNodeInfo->item()))
        {
            ListProperty* listProp = dynamic_cast<ListProperty*>(item);
            if(listProp)
            {
                if(listProp->listPropertyName() == parentProperty.name())
                {
                    info->item()->setParentItem(listProp);
                    break;
                }
            }
        }
    }
}



QList<QQuickItem*> DataflowDesignerView::getAllDescendands(QQuickItem *item)
{
    QList<QQuickItem*> children = item->childItems();
    foreach (QQuickItem* child, children) {
        QList<QQuickItem*> list = getAllDescendands(child);
        children.append(list);
    }
    return children;
}
void DataflowDesignerView::deselectAll()
{
    Q_D(DataflowDesignerView);
    foreach (NodeInfoPointer info, d->_IdInfo.values()) {
        info->setSelected(false);
    }
}

void DataflowDesignerView::onSelectedChanged()
{
    Q_D(DataflowDesignerView);
    NodeInfo* sender = (NodeInfo*)QObject::sender();
    QList<ModelNode> selectedNodes;
    if(sender->selected())
    {
        ModelNode node = modelNodeForInternalId(sender->internalId());
        selectedNodes.append(node);
        foreach(NodeInfoPointer info, d->_IdInfo.values())
        {
            if(info != sender) info->setSelected(false);
        }
    }
    QObject::disconnect(sender, SIGNAL(selectedChanged()), this, SLOT(onSelectedChanged()));
    d->_center = false;
    setSelectedModelNodes(selectedNodes);
    d->_center = true;
    QObject::connect(sender, SIGNAL(selectedChanged()), this, SLOT(onSelectedChanged()));
}

void DataflowDesignerView::nodeRemoved(const ModelNode &/*removedNode*/,
                                       const NodeAbstractProperty &/*parentProperty*/,
                                       PropertyChangeFlags /*propertyChange*/)
{
}
void DataflowDesignerView::selectedNodesChanged(const QList<ModelNode> &selectedNodeList,
                                                const QList<ModelNode> &/*lastSelectedNodeList*/)
{
    Q_D(DataflowDesignerView);
    NodeInfoPointer info;
    foreach (ModelNode node, selectedNodeList) {
        if(d->_IdInfo.contains(node.internalId()))
        {
            info = d->_IdInfo[node.internalId()];
            if(info) info->setSelected(true);
        }
    }
    if(d->_center && info)
    {
        d->_widget->graphicsView()->centerOn(info->item());
    }
}
void DataflowDesignerView::setCenter(bool center)
{
    Q_D(DataflowDesignerView);
    d->_center = center;
}

void DataflowDesignerView::nodeIdChanged(const ModelNode& node, const QString& newId, const QString& /*oldId*/)
{
    Q_D(DataflowDesignerView);
    NodeInfoPointer nodeInfo = d->_IdInfo[node.internalId()];
    nodeInfo->setNodeId(newId);
}

void DataflowDesignerView::nodeAboutToBeRemoved(const ModelNode &removedNode)
{
    Q_D(DataflowDesignerView);
    NodeInfoPointer nodeInfo = d->_IdInfo[removedNode.internalId()];
    d->scene()->childItems().removeOne(nodeInfo->item());
    nodeInfo->item()->setParentItem(NULL);
    delete nodeInfo->item();
    d->_IdInfo.remove(removedNode.internalId());
}
void DataflowDesignerView::onDraggingOutputPin(OutputPin *outputPin, QDragMoveEvent *event)
{
    Q_D(DataflowDesignerView);
    if(!d->_draggingConnection)
    {
        d->_draggingConnection.reset(new ConnectionInfo());
        d->_draggingConnection->setSignalPin(outputPin);
        QUrl url("qrc:/dataflow/specifics/DirectConnectionSpecifics.qml");
        QQmlComponent* component = new QQmlComponent(d->_engine.data(), url);
        QQmlContext* context = new QQmlContext(d->_engine.data());
        context->setContextProperty("node", d->_draggingConnection.data());
        QObject *obj = component->create(context);
        QQuickItem *item = qobject_cast<QQuickItem*>(obj);
        d->_draggingConnection->setItem(item);
        d->scene()->addItem(item);
    }
    if(!d->_draggingConnection->slotPin())
    {
        d->_draggingConnection->setEndPoint(event->posF());
    }
}
void DataflowDesignerView::onDraggingOutputPinFinished()
{
    Q_D(DataflowDesignerView);
    if(d->_draggingConnection)
        {
            d->_draggingConnection->item()->setParentItem(NULL);
            delete d->_draggingConnection->item();
            d->_draggingConnection = QSharedPointer<ConnectionInfo>();
        }
}
void DataflowDesignerView::onInputPinLeave(InputPin *pin)
{
    Q_D(DataflowDesignerView);
    if(d->_draggingConnection && d->_draggingConnection->slotPin() == pin)
    {
        d->_draggingConnection->setSlotPin(NULL);
    }
}
void DataflowDesignerView::onInputPinEnter(InputPin *inputPin)
{
    Q_D(DataflowDesignerView);
    if(d->_draggingConnection)
    {
        d->_draggingConnection->setSlotPin(inputPin);
    }
}
void DataflowDesignerView::onConnectionEstablished(InputPin *inputPin)
{
    Q_D(DataflowDesignerView);
    if(d->_draggingConnection)
    {
        d->_draggingConnection->setSlotPin(inputPin);
        //_draggingConnection.reset(NULL);
    }
}



void DataflowDesignerView::nodeAboutToBeReparented(const ModelNode &node,
                                                   const NodeAbstractProperty &newPropertyParent,
                                                   const NodeAbstractProperty &/*oldPropertyParent*/,
                                                   AbstractView::PropertyChangeFlags /*propertyChange*/)
{
    Q_D(DataflowDesignerView);
    ModelNode parent = newPropertyParent.parentModelNode();
    NodeInfoPointer parentNodeInfo = d->_IdInfo[parent.internalId()];
    ListProperty* listProp = parentNodeInfo->listProperty(newPropertyParent.name());
    NodeInfoPointer nodeInfo = d->_IdInfo[node.internalId()];
    nodeInfo->item()->setParentItem(listProp);
}
void DataflowDesignerView::nodeReparented(const ModelNode &/*node*/, const NodeAbstractProperty &/*newPropertyParent*/, const NodeAbstractProperty &/*oldPropertyParent*/, AbstractView::PropertyChangeFlags /*propertyChange*/){}
void DataflowDesignerView::propertiesAboutToBeRemoved(const QList<AbstractProperty>& /*propertyList*/) {}
void DataflowDesignerView::propertiesRemoved(const QList<AbstractProperty>& /*propertyList*/) {}
void DataflowDesignerView::variantPropertiesChanged(const QList<VariantProperty>& /*propertyList*/, PropertyChangeFlags /*propertyChange*/) {}
void DataflowDesignerView::bindingPropertiesChanged(const QList<BindingProperty>& /*propertyList*/, PropertyChangeFlags /*propertyChange*/) {}
void DataflowDesignerView::signalHandlerPropertiesChanged(const QVector<SignalHandlerProperty> & /*propertyList*/, AbstractView::PropertyChangeFlags /*propertyChange*/) {}
void DataflowDesignerView::rootNodeTypeChanged(const QString &/*type*/, int /*majorVersion*/, int /*minorVersion*/) {}
void DataflowDesignerView::scriptFunctionsChanged(const ModelNode &/*node*/, const QStringList &/*scriptFunctionList*/) {}
void DataflowDesignerView::instancePropertyChange(const QList<QPair<ModelNode, PropertyName> > &/*propertyList*/) {}
void DataflowDesignerView::instancesCompleted(const QVector<ModelNode> &/*completedNodeList*/) {}
void DataflowDesignerView::instanceInformationsChange(const QMultiHash<ModelNode, InformationName> &/*informationChangeHash*/) {}
void DataflowDesignerView::instancesRenderImageChanged(const QVector<ModelNode> &/*nodeList*/) {}
void DataflowDesignerView::instancesPreviewImageChanged(const QVector<ModelNode> &/*nodeList*/) {}
void DataflowDesignerView::instancesChildrenChanged(const QVector<ModelNode> &/*nodeList*/) {}
void DataflowDesignerView::instancesToken(const QString &/*tokenName*/, int /*tokenNumber*/, const QVector<ModelNode> &/*nodeVector*/) {}
void DataflowDesignerView::nodeSourceChanged(const ModelNode &, const QString & /*newNodeSource*/) {}
void DataflowDesignerView::rewriterBeginTransaction() {}
void DataflowDesignerView::rewriterEndTransaction() {}
void DataflowDesignerView::currentStateChanged(const ModelNode &/*node*/) {}
void DataflowDesignerView::fileUrlChanged(const QUrl &/*oldUrl*/, const QUrl &/*newUrl*/) {}
void DataflowDesignerView::nodeOrderChanged(const NodeListProperty &/*listProperty*/, const ModelNode & /*movedNode*/, int /*oldIndex*/) {}
void DataflowDesignerView::auxiliaryDataChanged(const ModelNode &/*node*/, const PropertyName &/*name*/, const QVariant &/*data*/) {}
void DataflowDesignerView::customNotification(const AbstractView * /*view*/, const QString &/*identifier*/, const QList<ModelNode> &/*nodeList*/, const QList<QVariant> &/*data*/) {}
void DataflowDesignerView::importsChanged(const QList<Import> &/*addedImports*/, const QList<Import> &/*removedImports*/) {}

