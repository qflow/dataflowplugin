import qbs 1.0

QtcPlugin {
    name: "DataflowPlugin"

    Depends { name: "Qt"; submodules: ["widgets", "quick", "qml"] }
    Depends { name: "Utils" }

    Depends { name: "Core" }
    Depends { name: "QmlDesigner" }

    cpp.cppFlags: ["-U QT_NO_CAST_FROM_ASCII"]
    files: ["dataflowplugin.qrc"]

    cpp.includePaths: base.concat([
        "../qmldesigner/designercore",
        "../qmldesigner/designercore/include",
        "../qmldesigner/components/integration",
        "../../../share/qtcreator/qml/qmlpuppet/interfaces",
        "../../../share/qtcreator/qml/qmlpuppet/types",
        "../qmldesigner/components/componentcore",
        /*"designercore/instances",
        "../../../share/qtcreator/qml/qmlpuppet/container",
        "../../../share/qtcreator/qml/qmlpuppet/commands",
        "components/propertyeditor",
        "components/formeditor",
        "components/itemlibrary",
        "components/navigator",
        "components/pluginmanager",
        "components/stateseditor",
        "components/importmanager",
        "components/debugview",
        "."*/
    ])
    Group
    {
        name: "headers"
        files:[
            "zoomaction.h",
                //"module.h",
                "nodeinfo.h",
                "basicitem.h",
                "triangle.h",
                "inputpin.h",
                "outputpin.h",
                "pin.h",
                "connectioninfo.h",
                "dataflowplugin_global.h",
                "dataflowpluginconstants.h",
                "dataflowplugin.h",
                "objectmimedata.h",
                "dataflowcontext.h",
                "librarynodeinfo.h",
                "listproperty.h",
                "dataflowdesignerview.h",
                "dataflowdesignerwidget.h",
                "dataflowgraphscene.h",
                "dataflowgraphicsview.h",
                "nodeinfo_p.h",
                "basicitem_p.h",
                "bezier.h",
                "pin_p.h"
        ]
    }
    Group
    {
        name: "sources"
        files:[
            "zoomaction.cpp",
                //"module.cpp",
                "nodeinfo.cpp",
                "basicitem.cpp",
                "triangle.cpp",
                "inputpin.cpp",
                "outputpin.cpp",
                "pin.cpp",
                "connectioninfo.cpp",
                "dataflowplugin.cpp",
                "objectmimedata.cpp",
                "dataflowcontext.cpp",
                "librarynodeinfo.cpp",
                "listproperty.cpp",
                "dataflowdesignerview.cpp",
                "dataflowdesignerwidget.cpp",
                "dataflowgraphscene.cpp",
                "dataflowgraphicsview.cpp",
                "bezier.cpp"
        ]
    }

}

