#include "dataflowgraphicsview.h"
#include "dataflowgraphscene.h"
#include <QDebug>

class DataflowGraphicsViewPrivate
{
public:
    DataflowGraphScene* _scene;
    QSizeF _originalSize;
};

DataflowGraphScene* findScene(QQuickItem* item)
{
    foreach (QQuickItem* child, item->childItems()) {
        DataflowGraphScene* scene = findScene(child);
        if(scene) return scene;
    }
    DataflowGraphScene* thisScene = qobject_cast<DataflowGraphScene*>(item);
    return thisScene;
}

DataflowGraphicsView::DataflowGraphicsView(QQmlEngine *engine, QWindow *parent) :
    QQuickView(engine, parent), d_ptr(new DataflowGraphicsViewPrivate)
{
    Q_D(DataflowGraphicsView);
    QUrl url("qrc:/dataflow/specifics/MainCanvas.qml");
    setColor(Qt::black);
    setSource(url);
    setResizeMode(QQuickView::SizeRootObjectToView);
    d->_scene = findScene(rootObject());
    d->_originalSize = QSizeF(d->_scene->width(), d->_scene->height());
}
DataflowGraphicsView::~DataflowGraphicsView()
{

}
DataflowGraphScene* DataflowGraphicsView::scene() const
{
    Q_D(const DataflowGraphicsView);
    return d->_scene;
}

bool DataflowGraphicsView::eventFilter(QObject *obj, QEvent *event)
{
    Q_D(DataflowGraphicsView);
    if(event->type() == QEvent::DragEnter)
    {
        QDragEnterEvent* dragEnterEvent = (QDragEnterEvent*)event;
        d->_scene->dragEnterEvent(dragEnterEvent);
    }
    if(event->type() == QEvent::DragMove)
    {
        QDragMoveEvent* dragMoveEvent = (QDragMoveEvent*)event;
        d->_scene->dragMoveEvent(dragMoveEvent);
    }
    return QObject::eventFilter(obj, event);
}
void DataflowGraphicsView::centerOn(QPointF point)
{
    Q_D(DataflowGraphicsView);
    QPointF scenePos = d->_scene->scale() * point;
    QPointF rootObjectCenter = rootObject()->boundingRect().center();
    QPointF center = -scenePos + rootObjectCenter;
    d->_scene->setPosition(center);
}

void DataflowGraphicsView::centerOn(QQuickItem* item)
{
    Q_D(DataflowGraphicsView);
    QPointF itemScenePos = item->mapToItem(d->_scene, item->boundingRect().center());
    centerOn(itemScenePos);
}
void DataflowGraphicsView::wheelEvent(QWheelEvent *event)
{
    emit onWheel(event);
    QQuickView::wheelEvent(event);
}
void DataflowGraphicsView::setZoomLevel(double value)
{
    Q_D(DataflowGraphicsView);
    QPointF center = -d->_scene->position() + rootObject()->boundingRect().center();
    center /= d->_scene->scale();
    d->_scene->setTransformOrigin(QQuickItem::TopLeft);
    d->_scene->setScale(value);
    centerOn(center);
}
