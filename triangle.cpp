#include "triangle.h"
#include <QSGGeometryNode>
#include <QSGFlatColorMaterial>

Triangle::Triangle(QQuickItem *parent) :
    QQuickItem(parent), _orientation(Right)
{
    setFlag(QQuickItem::ItemHasContents);
}

QSGNode* Triangle::updatePaintNode(QSGNode *oldNode, UpdatePaintNodeData * /*data*/)
{
    QSGGeometryNode *node = 0;
    QSGGeometry *geometry = 0;

    if (!oldNode) {
        node = new QSGGeometryNode;
        geometry = new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), 3);
        geometry->setLineWidth(2);
        geometry->setDrawingMode(GL_TRIANGLE_STRIP);
        node->setGeometry(geometry);
        node->setFlag(QSGNode::OwnsGeometry);
        QSGFlatColorMaterial *material = new QSGFlatColorMaterial;
        material->setColor(_color);
        node->setMaterial(material);
        node->setFlag(QSGNode::OwnsMaterial);
        geometry->allocate(3);
    } else {
        node = static_cast<QSGGeometryNode *>(oldNode);
        geometry = node->geometry();
    }
    QSGGeometry::Point2D *vertices = geometry->vertexDataAsPoint2D();

    if(_orientation == Right)
    {
        vertices[0].set(0,0);
        vertices[1].set(0,height());
        vertices[2].set(width(), height()/2);
    }
    if(_orientation == Top)
    {
        vertices[0].set(0,height());
        vertices[1].set(width(),height());
        vertices[2].set(width()/2, 0);
    }

    if(_orientation == Bottom)
    {
        vertices[0].set(0,0);
        vertices[1].set(width(),0);
        vertices[2].set(width()/2, height());
    }
    if(_orientation == Left)
    {
        vertices[0].set(width(),0);
        vertices[1].set(width(),height());
        vertices[2].set(0, height()/2);
    }
    node->markDirty(QSGNode::DirtyGeometry);
    return node;
}

bool Triangle::contains(const QPointF & point) const
{
    return QQuickItem::contains(point);
}
void Triangle::setColor(QColor val)
{
    _color = val;
    emit colorChanged();
}
QColor Triangle::color()
{
    return _color;
}

