#ifndef PIN_H
#define PIN_H

#include <QQuickItem>

class DataflowGraphScene;

class PinPrivate;
class Pin : public QQuickItem
{
    Q_OBJECT
    Q_PROPERTY(QPointF scenePosition READ scenePosition NOTIFY scenePositionChanged)
    Q_PROPERTY(QPointF hotspot READ hotspot WRITE setHotspot NOTIFY hotspotChanged)
    Q_PROPERTY(QPointF hotspotScenePosition READ hotspotScenePosition NOTIFY hotspotScenePositionChanged)
protected:
    virtual void itemChange(ItemChange change, const ItemChangeData & value);
    virtual void hoverEnterEvent(QHoverEvent *event);
    virtual void hoverLeaveEvent(QHoverEvent *event);
    DataflowGraphScene* scene() const;
public:
    explicit Pin(QQuickItem *parent = 0);
    ~Pin();
    QPointF hotspot() const;
    void setHotspot(QPointF point);
    QPointF hotspotScenePosition() const;
    QPointF scenePosition() const;
signals:
    void scenePositionChanged();
    void hotspotChanged();
    void hotspotScenePositionChanged();
    void hoverEnter();
    void hoverLeave();
public slots:
protected:
    const QScopedPointer<PinPrivate> d_ptr;
    Pin(PinPrivate &dd, QQuickItem *parent);
private:
    void init();
    Q_DECLARE_PRIVATE(Pin)
};

#endif // PIN_H
