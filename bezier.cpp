#include "bezier.h"
#include <QSGGeometryNode>
#include <QSGFlatColorMaterial>
#include <qmath.h>

class BezierPrivate
{
public:
    QColor _color;
    QPointF _startPoint;
    QPointF _endPoint;
    QPointF _startControlPoint;
    QPointF _endControlPoint;
    int m_segmentCount;
    QSGGeometry* _geometry;
    QSGFlatColorMaterial *_material;
    BezierPrivate() : _color(Qt::red),
        m_segmentCount(32), _geometry(NULL), _material(NULL)
    {

    }
    QRectF rect() const;
    QRectF extendedBoundingRect() const;
};

Bezier::Bezier(QQuickItem *parent) :
    QQuickItem(parent), d_ptr(new BezierPrivate())
{
    setFlag(ItemHasContents);
    setFlag(ItemAcceptsDrops, false);
    setAcceptedMouseButtons(Qt::LeftButton);
}
Bezier::~Bezier()
{

}

bool Bezier::contains(const QPointF & point) const
{
    Q_D(const Bezier);
    QVector2D p(point);
    QSGGeometry::Point2D *vertices = d->_geometry->vertexDataAsPoint2D();
    qreal  min;
    bool found = false;
    for(int i=0;i<d->m_segmentCount-2;i++)
    {
        QVector2D start(vertices[i].x, vertices[i].y);
        QVector2D end(vertices[i+1].x, vertices[i+1].y);

        qreal l2 = (end- start).lengthSquared();
        qreal t = QVector2D::dotProduct(p - start, end - start) / l2;
        if(t >=0 && t <= 1)
        {
            QVector2D projection = start + t * (end - start);
            qreal dist = p.distanceToPoint(projection);
            if(!found)
            {
                min = dist;
                found = true;
            }
            if(dist < min) min = dist;
        }
    }
    if(found && min < 8)
    {
        return true;
    }
    return false;
}

QColor Bezier::color()
{
    Q_D(Bezier);
    return d->_color;
}
void Bezier::setColor(QColor color)
{
    Q_D(Bezier);
    d->_color = color;
    emit colorChanged();
    update();
}
QPointF Bezier::startPoint()
{
    Q_D(Bezier);
    return d->_startPoint;
}
void Bezier::setStartPoint(QPointF startPoint)
{
    Q_D(Bezier);

    d->_startPoint = startPoint;
    QRectF bounds = d->extendedBoundingRect();
    setPosition(bounds.topLeft());
    setSize(bounds.size());
    emit startPointChanged();
}
QPointF Bezier::endPoint()
{
    Q_D(Bezier);
    return d->_endPoint;
}
void Bezier::setEndPoint(QPointF endPoint)
{
    Q_D(Bezier);
    d->_endPoint = endPoint;
    QRectF bounds = d->extendedBoundingRect();
    setPosition(bounds.topLeft());
    setSize(bounds.size());
    emit endPointChanged();
}
void Bezier::setStartControlPoint(QPointF point)
{
    Q_D(Bezier);
    d->_startControlPoint = point;
    emit startControlPointChanged();
}
QPointF Bezier::startControlPoint()
{
    Q_D(Bezier);
    return d->_startControlPoint;
}
void Bezier::setEndControlPoint(QPointF point)
{
    Q_D(Bezier);
    d->_endControlPoint = point;
    emit endControlPointChanged();
}
QPointF Bezier::endControlPoint()
{
    Q_D(Bezier);
    return d->_endControlPoint;
}
QRectF BezierPrivate::rect() const
{
    QPoint topLeft(qMin(_startPoint.x(), _endPoint.x()),
                        qMin(_startPoint.y(), _endPoint.y()));
    QPoint bottomRight(qMax(_startPoint.x(), _endPoint.x()),
                            qMax(_startPoint.y(), _endPoint.y()));
    return QRectF(topLeft, bottomRight);
}
QRectF BezierPrivate::extendedBoundingRect() const
{
    QRectF bounds = rect();
    QRectF ext = bounds.adjusted(0, -10, 0, 10);
    return ext;
}
QSGNode* Bezier::updatePaintNode(QSGNode *oldNode, UpdatePaintNodeData */*data*/)
{
    Q_D(Bezier);
    QSGGeometryNode *node = 0;

    if (!oldNode) {
        node = new QSGGeometryNode;
        d->_geometry = new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), d->m_segmentCount);
        d->_geometry->setLineWidth(2);
        d->_geometry->setDrawingMode(GL_LINE_STRIP);
        node->setGeometry(d->_geometry);
        node->setFlag(QSGNode::OwnsGeometry);
        d->_material = new QSGFlatColorMaterial;
        node->setMaterial(d->_material);
        node->setFlag(QSGNode::OwnsMaterial);
        d->_geometry->allocate(d->m_segmentCount);
    } else {
        node = static_cast<QSGGeometryNode *>(oldNode);
    }
    d->_material->setColor(d->_color);

    QSGGeometry::Point2D *vertices = d->_geometry->vertexDataAsPoint2D();
    for (int i = 0; i < d->m_segmentCount; ++i) {
        qreal t = i / qreal(d->m_segmentCount - 1);
        qreal invt = 1 - t;

        QPointF p1 = mapFromItem(parentItem(), d->_startPoint);
        QPointF p2 = mapFromItem(parentItem(), d->_startControlPoint);
        QPointF p3 = mapFromItem(parentItem(), d->_endControlPoint);
        QPointF p4 = mapFromItem(parentItem(), d->_endPoint);

        QPointF pos = invt * invt * invt * p1
                + 3 * invt * invt * t * p2
                + 3 * invt * t * t * p3
                + t * t * t * p4;

        vertices[i].set(pos.x(), pos.y());
    }
    node->markDirty(QSGNode::DirtyGeometry);
    return node;
}
void Bezier::mousePressEvent(QMouseEvent *event)
{
    event->accept();
    emit pressed();
}
