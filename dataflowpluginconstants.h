#ifndef DATAFLOWPLUGINCONSTANTS_H
#define DATAFLOWPLUGINCONSTANTS_H

namespace Dataflow {
namespace Constants {

const char ACTION_ID[] = "DataflowPlugin.Action";
const char MENU_ID[] = "DataflowPlugin.Menu";

} // namespace Dataflow
} // namespace Constants

#endif // DATAFLOWPLUGINCONSTANTS_H

