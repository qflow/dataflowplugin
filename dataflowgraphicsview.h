#ifndef DATAFLOWGRAPHICSVIEW_H
#define DATAFLOWGRAPHICSVIEW_H

#include <QQuickView>

class DataflowGraphScene;

class DataflowGraphicsViewPrivate;
class DataflowGraphicsView : public QQuickView
{
    Q_OBJECT
protected:
    bool eventFilter(QObject *obj, QEvent *event);
    void wheelEvent(QWheelEvent * event);
public:
    explicit DataflowGraphicsView(QWindow *parent = 0);
    DataflowGraphicsView(QQmlEngine* engine, QWindow *parent);
    ~DataflowGraphicsView();
    DataflowGraphScene* scene() const;
    void centerOn(QQuickItem* item);
    void centerOn(QPointF point);
    void setZoomLevel(double value);
signals:
    void onWheel(QWheelEvent * event);
public slots:
private:
    const QScopedPointer<DataflowGraphicsViewPrivate> d_ptr;
    Q_DECLARE_PRIVATE(DataflowGraphicsView)
};

#endif // DATAFLOWGRAPHICSVIEW_H
