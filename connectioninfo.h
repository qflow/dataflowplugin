#ifndef CONNECTIONINFO_H
#define CONNECTIONINFO_H

#include <QObject>
#include "inputpin.h"
#include "outputpin.h"
#include <modelnode.h>
#include "nodeinfo.h"

using namespace QmlDesigner;

class ConnectionInfoPrivate;
class ConnectionInfo : public NodeInfo
{
    Q_OBJECT
    Q_PROPERTY(InputPin* slotPin READ slotPin WRITE setSlotPin NOTIFY slotPinChanged)
    Q_PROPERTY(OutputPin* signalPin READ signalPin WRITE setSignalPin NOTIFY signalPinChanged)
    Q_PROPERTY(QPointF endPoint READ endPoint WRITE setEndPoint NOTIFY endPointChanged)
    Q_PROPERTY(QPointF startPoint READ startPoint WRITE setStartPoint NOTIFY startPointChanged)
private slots:
    void onOutPinHotspotChanged();
    void onInPinHotspotChanged();
public:
    explicit ConnectionInfo(QObject *parent = 0);
    ~ConnectionInfo();
    InputPin* slotPin() const;
    void setSlotPin(InputPin* inputPin);
    OutputPin* signalPin() const;
    void setSignalPin(OutputPin* outputPin);
    void setEndPoint(QPointF point);
    QPointF endPoint() const;
    void setStartPoint(QPointF point);
    QPointF startPoint() const;
signals:
    void slotPinChanged();
    void signalPinChanged();
    void endPointChanged();
    void startPointChanged();
public slots:
protected:
    ConnectionInfo(ConnectionInfoPrivate &dd, QObject *parent);
private:
    Q_DECLARE_PRIVATE(ConnectionInfo)
    
};

#endif // CONNECTIONINFO_H
