#include "nodeinfo.h"
#include "nodeinfo_p.h"
#include <nodeproperty.h>
#include <variantproperty.h>
#include <nodemetainfo.h>
#include <abstractproperty.h>
#include "dataflowdesignerview.h"
#include <QQmlContext>
#include "listproperty.h"



NodeInfo::NodeInfo(QObject *parent) :
    QObject(parent), d_ptr(new NodeInfoPrivate())
{
}
NodeInfo::NodeInfo(NodeInfoPrivate &dd, QObject *parent) :
    QObject(parent), d_ptr(&dd)
{

}
NodeInfoPointer NodeInfo::parentNodeInfo()
{
    Q_D(NodeInfo);
    ModelNode modelNode = this->modelNode();
    if(!modelNode.hasParentProperty()) return NodeInfoPointer();
    NodeAbstractProperty parentProperty = modelNode.parentProperty();
    ModelNode parent = parentProperty.parentModelNode();
    NodeInfoPointer parentNodeInfo = d->_view->nodeInfo(parent.internalId());
    return parentNodeInfo;
}

NodeInfo::~NodeInfo()
{

}
ModelNode NodeInfo::modelNode() const
{
    Q_D(const NodeInfo);
    ModelNode node = d->_view->modelNodeForInternalId(d->_internalId);
    return node;
}

QString NodeInfo::nodeId() const
{
    Q_D(const NodeInfo);
    return d->_nodeId;
}
void NodeInfo::setNodeId(QString value)
{
    Q_D(NodeInfo);
    d->_nodeId = value;
    emit nodeIdChanged();
}

bool NodeInfo::selected() const
{
    Q_D(const NodeInfo);
    return d->_selected;
}
void NodeInfo::setSelected(bool value)
{
    Q_D(NodeInfo);
    d->_selected = value;
    emit selectedChanged();
}
void NodeInfo::setInternalId(quint32 id)
{
    Q_D(NodeInfo);
    d->_internalId = id;
}
quint32 NodeInfo::internalId() const
{
    Q_D(const NodeInfo);
    return d->_internalId;
}
QString NodeInfo::typeName() const
{
    return modelNode().metaInfo().typeName();
}
void NodeInfo::setView(DataflowDesignerView *view)
{
    Q_D(NodeInfo);
    d->_view = view;
}
QVariant NodeInfo::propertyValue(QString propertyName)
{
    NodeProperty prop = modelNode().nodeProperty(propertyName.toLatin1());
    VariantProperty varProp = prop.toVariantProperty();
    return varProp.value();
}
QQmlListProperty<NodeInfo> NodeInfo::children()
{
    Q_D(NodeInfo);
    QList<NodeInfo*> list;
    foreach(ModelNode childNode, modelNode().allSubModelNodes())
    {
        NodeInfoPointer childInfo = d->_view->nodeInfo(childNode.internalId());
        if(childInfo) list.append(childInfo.data());
    }
    return QQmlListProperty<NodeInfo>(this, list);
}
QQuickItem* NodeInfo::item() const
{
    Q_D(const NodeInfo);
    return d->_item;
}
void NodeInfo::setItem(QQuickItem *item)
{
    Q_D(NodeInfo);
    d->_item = item;
    emit itemChanged();
}
void NodeInfo::setContext(QQmlContext *context)
{
    Q_D(NodeInfo);
    d->_context = context;
}
QQmlContext* NodeInfo::context() const
{
    Q_D(const NodeInfo);
    return d->_context;
}
ListProperty* NodeInfo::listProperty(QString name)
{
    Q_D(NodeInfo);
    QList<QQuickItem*> items =  DataflowDesignerView::getAllDescendands(d->_item);
    foreach(QQuickItem* item, items)
    {
        ListProperty* listProp = dynamic_cast<ListProperty*>(item);
        if(listProp)
        {
            if(listProp->listPropertyName() == name) return listProp;
        }
    }
    return NULL;
}

