#ifndef NODEINFO_P_H
#define NODEINFO_P_H

#include "dataflowdesignerview.h"
#include <QQmlContext>

class NodeInfoPrivate
{
public:
    bool _selected;
    quint32 _internalId;
    DataflowDesignerView* _view;
    QQuickItem* _item;
    QQmlContext* _context;
    QString _nodeId;
    NodeInfoPrivate() : _selected(false), _item(NULL), _context(NULL)
    {

    }
    virtual ~NodeInfoPrivate()
    {
        if(_context) delete _context;
    }
};

#endif // NODEINFO_P_H
