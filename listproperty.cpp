#include "listproperty.h"
#include <nodeproperty.h>
#include <modelnode.h>

class ListPropertyPrivate
{
public:
    QString _listPropertyName;
};

ListProperty::ListProperty(QQuickItem *parent) : BasicItem(parent), d_ptr(new ListPropertyPrivate())
{
}
ListProperty::~ListProperty()
{

}

QString ListProperty::listPropertyName() const
{
    Q_D(const ListProperty);
    return d->_listPropertyName;
}
void ListProperty::setListPropertyName(QString value)
{
    Q_D(ListProperty);
    d->_listPropertyName = value;
    emit listPropertyNameChanged();
}
NodeListProperty ListProperty::nodeListProperty()
{
    Q_D(ListProperty);
    ModelNode node = modelNode();
    NodeListProperty prop = node.nodeListProperty(d->_listPropertyName.toLatin1());
    if(prop.isValid()) return prop;
    foreach (NodeListProperty listProp, node.nodeListProperties()) {
        if(listProp.name() == d->_listPropertyName) return listProp;
    }
    if(node.hasDefaultNodeAbstractProperty())
    {
        NodeAbstractProperty prop = node.defaultNodeAbstractProperty();
        NodeListProperty defaultProperty = prop.toNodeListProperty();
        if(defaultProperty.isValid())
        {
            return defaultProperty;
        }
    }
    return NodeListProperty();
}
