#ifndef DATAFLOWGRAPHSCENE_H
#define DATAFLOWGRAPHSCENE_H

#include <QQuickItem>

class DataflowGraphicsView;
class DataflowDesignerView;
class InputPin;

class DataflowGraphScenePrivate;
class DataflowGraphScene : public QQuickItem
{
    Q_OBJECT
    friend class DataflowGraphicsView;
protected:
    void mouseMoveEvent(QMouseEvent* event);
    void mousePressEvent(QMouseEvent * event);
    void mouseDoubleClickEvent(QMouseEvent * event);
    void wheelEvent(QWheelEvent * event);
    void geometryChanged(const QRectF &newGeometry, const QRectF &oldGeometry);
    void dragEnterEvent(QDragEnterEvent * event);
    void dragMoveEvent(QDragMoveEvent * event);
public:
    explicit DataflowGraphScene(QQuickItem *parent = 0);
    ~DataflowGraphScene();
    void addItem(QQuickItem* item);
    static void updatePins(QQuickItem *item);
    void setView(DataflowDesignerView* value);
    QList<QQuickItem*> itemsAt(QPointF pos);
signals:

public slots:
    void onInputPinLeave(InputPin* pin);
    void onConnectionEstablished(InputPin* pin);
    void onInputPinEnter(InputPin* pin);
private:
    const QScopedPointer<DataflowGraphScenePrivate> d_ptr;
    Q_DECLARE_PRIVATE(DataflowGraphScene)

};

#endif // DATAFLOWGRAPHSCENE_H
