#include "dataflowdesignerwidget.h"
#include "dataflowgraphscene.h"
#include "dataflowgraphicsview.h"
#include <QVBoxLayout>
#include <utils/fileutils.h>
#include "dataflowdesignerview.h"
#include <QScrollArea>
#include <QActionGroup>
//#include <toolbox.h>
#include <QToolBar>
#include <QQuickView>

class DataflowDesignerWidgetPrivate
{
public:
    DataflowDesignerView* _view;
    QActionGroup* m_toolActionGroup;
    QAction* m_resetAction;
    ZoomAction* m_zoomAction;
    QToolBar* _toolBar;
    DataflowGraphicsView* _graphicsView;
    QWidget* _container;
};

DataflowDesignerWidget::DataflowDesignerWidget(DataflowDesignerView *view, QQmlEngine* engine) :
    QWidget(), d_ptr(new DataflowDesignerWidgetPrivate())
{
    Q_D(DataflowDesignerWidget);
    d->_view = view;
    setAcceptDrops(true);
    setStyleSheet(QLatin1String(Utils::FileReader::fetchQrc(tr(":/qmldesigner/formeditorstylesheet.css"))));
    QVBoxLayout *fillLayout = new QVBoxLayout(this);
    fillLayout->setMargin(0);
    fillLayout->setSpacing(0);
    setLayout(fillLayout);

    d->_toolBar = new QToolBar(this);
    fillLayout->addWidget(d->_toolBar);
    QWidget *separator = new QWidget(d->_toolBar);
    separator->setSizePolicy(QSizePolicy::Expanding,
                             QSizePolicy::Expanding);
    d->_toolBar->addWidget(separator);


    d->_graphicsView = new DataflowGraphicsView(engine, NULL);
    connect(d->_graphicsView, SIGNAL(onWheel(QWheelEvent*)), this, SLOT(onWheel(QWheelEvent*)));

    d->_container = QWidget::createWindowContainer(d->_graphicsView, this);
    d->_container->setAcceptDrops(true);
    d->_container->installEventFilter(d->_graphicsView);
    fillLayout->addWidget(d->_container);

    d->_container->setStyleSheet(
                QLatin1String(Utils::FileReader::fetchQrc(tr(":/qmldesigner/scrollbar.css"))));

    d->m_toolActionGroup = new QActionGroup(this);
    QList<QAction*> actions;

    d->m_zoomAction = new ZoomAction(d->m_toolActionGroup);
    actions.push_back(d->m_zoomAction);

    connect(d->m_zoomAction, SIGNAL(zoomLevelChanged(double)), SLOT(setZoomLevel(double)));
    addAction(d->m_zoomAction);

    d->m_resetAction = new QAction(QPixmap(tr(":/icon/reset.png")), tr("Reset view (R)."), d->m_toolActionGroup);
    d->m_resetAction->setShortcut(Qt::Key_R);
    d->m_resetAction->setShortcutContext(Qt::WidgetWithChildrenShortcut);
    addAction(d->m_resetAction);
    actions.push_back(d->m_resetAction);
    connect(d->m_resetAction, SIGNAL(triggered(bool)), this, SLOT(resetNodeInstanceView()));
    d->_toolBar->addActions(actions);

}
void DataflowDesignerWidget::resetNodeInstanceView()
{
    //_view->setCurrentState(_view->baseState());
    //_view->emitCustomNotification(QLatin1String("reset QmlPuppet"));
}
void DataflowDesignerWidget::setZoomLevel(double value)
{
    Q_D(DataflowDesignerWidget);
    d->_graphicsView->setZoomLevel(value);
}
DataflowDesignerView* DataflowDesignerWidget::view() const
{
    Q_D(const DataflowDesignerWidget);
    return d->_view;
}
DataflowGraphicsView* DataflowDesignerWidget::graphicsView() const
{
    Q_D(const DataflowDesignerWidget);
    return d->_graphicsView;
}


DataflowDesignerWidget::~DataflowDesignerWidget()
{
}
void DataflowDesignerWidget::onWheel(QWheelEvent * event)
{
    Q_D(DataflowDesignerWidget);
    int delta = event->angleDelta().y();
    if(delta > 0)
    {
        d->m_zoomAction->zoomIn();
    }
    else
    {
        d->m_zoomAction->zoomOut();
    }
}

QString DataflowDesignerWidget::contextHelpId() const
{
    Q_D(const DataflowDesignerWidget);
    if (!d->_view)
        return QString();

    QList<ModelNode> nodes = d->_view->selectedModelNodes();
    QString helpId;
    if (!nodes.isEmpty()) {
        helpId = nodes.first().type();
        helpId.replace("QtQuick", "QML");
    }

    return helpId;
}
