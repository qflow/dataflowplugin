#ifndef OBJECTMIMEDATA_H
#define OBJECTMIMEDATA_H

#include <QObject>
#include <QMimeData>

class ObjectMimeData : public QMimeData
{
protected:
    QObject* _object;
public:
    ObjectMimeData();
    ~ObjectMimeData();

    void setObject(QObject* object);
    QObject* getObject();

};

#endif // OBJECTMIMEDATA_H
