#ifndef LISTPROPERTY_H
#define LISTPROPERTY_H

#include "basicitem.h"
#include <nodelistproperty.h>

class ListPropertyPrivate;
class ListProperty : public BasicItem
{
    Q_OBJECT
    Q_PROPERTY(QString listPropertyName READ listPropertyName WRITE setListPropertyName NOTIFY listPropertyNameChanged)
public:
    ListProperty(QQuickItem *parent = 0);
    ~ListProperty();
    QString listPropertyName() const;
    void setListPropertyName(QString value);
    NodeListProperty nodeListProperty();
signals:
    void listPropertyNameChanged();
private:
    const QScopedPointer<ListPropertyPrivate> d_ptr;
    Q_DECLARE_PRIVATE(ListProperty)
};

#endif // LISTPROPERTY_H
