#include "zoomaction.h"
#include <QComboBox>

ZoomAction::ZoomAction(QObject *parent)
    :  QWidgetAction(parent),
      m_comboBoxModel(NULL),
    m_zoomLevel(1.0),
    m_currentComboBoxIndex(-1)
{

}

double ZoomAction::zoomLevel() const
{
    return m_zoomLevel;
}

void ZoomAction::zoomOut()
{
    int newIndex = m_currentComboBoxIndex + 1;
    if(newIndex >=0 && newIndex < m_comboBoxModel->rowCount())
        emit indexChanged(newIndex);

}

void ZoomAction::zoomIn()
{
    int newIndex = m_currentComboBoxIndex - 1;
    if(newIndex >=0 && newIndex < m_comboBoxModel->rowCount())
        emit indexChanged(newIndex);
}

void ZoomAction::setZoomLevel(double zoomLevel)
{
    if (zoomLevel < .1)
        m_zoomLevel = 0.1;
    else if (zoomLevel > 16.0)
        m_zoomLevel = 16.0;
    else
        m_zoomLevel = zoomLevel;

    emit zoomLevelChanged(m_zoomLevel);
}


QWidget *ZoomAction::createWidget(QWidget *parent)
{
    QComboBox *comboBox = new QComboBox(parent);

    if (!m_comboBoxModel) {
        m_comboBoxModel = comboBox->model();
        comboBox->addItem(tr("800 %"), 8.0);
        comboBox->addItem(tr("400 %"), 4.0);
        comboBox->addItem(tr("200 %"), 2.0);
        comboBox->addItem(tr("150 %"), 1.5);
        comboBox->addItem(tr("100 %"), 1.0);
        comboBox->addItem(tr("75 %"), 0.75);
        comboBox->addItem(tr("50 %"), 0.5);
        comboBox->addItem(tr("25 %"), 0.25);
        comboBox->addItem(tr("10 %"), 0.1);
    } else {
        comboBox->setModel(m_comboBoxModel);
    }
    m_currentComboBoxIndex = 4;
    comboBox->setCurrentIndex(m_currentComboBoxIndex);

    connect(comboBox, SIGNAL(currentIndexChanged(int)), SLOT(emitZoomLevelChanged(int)));
    connect(this, SIGNAL(indexChanged(int)), comboBox, SLOT(setCurrentIndex(int)));

    comboBox->setProperty("hideborder", true);
    return comboBox;
}

void ZoomAction::emitZoomLevelChanged(int index)
{
    m_currentComboBoxIndex = index;

    if (index == -1)
        return;

    QModelIndex modelIndex(m_comboBoxModel->index(index, 0));
    setZoomLevel(m_comboBoxModel->data(modelIndex, Qt::UserRole).toDouble());
}

