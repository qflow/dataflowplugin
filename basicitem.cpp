#include "basicitem.h"
#include "basicitem_p.h"
#include "pin.h"
#include <QQmlEngine>
#include <QQmlContext>
#include "nodeinfo.h"
#include <QCursor>
#include <QQuickWindow>


BasicItem::BasicItem(QQuickItem *parent) :
    QQuickItem(parent), d_ptr(new BasicItemPrivate())
{
    setCursor(QCursor(Qt::ArrowCursor));
    setFlags(QQuickItem::ItemHasContents);
    setMovable(true);
    setAcceptedMouseButtons( Qt::LeftButton );
}
BasicItem::BasicItem(BasicItemPrivate &dd, QQuickItem *parent) :
    QQuickItem(parent), d_ptr(&dd)
{

}
BasicItem::~BasicItem()
{

}

bool BasicItem::movable() const
{
    Q_D(const BasicItem);
    return d->_movable;
}
void BasicItem::setMovable(bool val)
{
    Q_D(BasicItem);
    d->_movable = val;
    emit movableChanged();
}
ModelNode BasicItem::modelNode() const
{
    QQmlContext* context = QQmlEngine::contextForObject(this);
    QVariant var = context->contextProperty("node");
    QObject* obj = qvariant_cast<QObject*>(var);
    NodeInfo* info = qobject_cast<NodeInfo*>(obj);
    return info->modelNode();
}
void BasicItem::updatePins(QQuickItem *item)
{
    foreach (QQuickItem* child, item->childItems()) {
        BasicItem::updatePins(child);
    }
    Pin* pin = qobject_cast<Pin*>(item);
    if(pin)
    {
        emit pin->scenePositionChanged();
        emit pin->hotspotScenePositionChanged();
    }
}
void BasicItem::geometryChanged(const QRectF & newGeometry, const QRectF & oldGeometry)
{
    BasicItem::updatePins(this);
    QQuickItem::geometryChanged(newGeometry, oldGeometry);
}
void BasicItem::mousePressEvent(QMouseEvent *event)
{
    Q_D(BasicItem);
    d->_previousScenePosition = QQuickItem::window()->mapFromGlobal(event->globalPos());
    event->accept();
    emit pressed();
}
void BasicItem::mouseReleaseEvent(QMouseEvent *event)
{
    QQuickItem::mouseReleaseEvent(event);
}
void BasicItem::mouseMoveEvent(QMouseEvent *event)
{
    Q_D(BasicItem);
    if(d->_movable && (event->buttons() & Qt::LeftButton))
    {
        event->accept();
        QPointF scenePos = QQuickItem::window()->mapFromGlobal(event->globalPos());
        QPointF delta = scenePos - d->_previousScenePosition;
        delta = delta / d->_scene->scale();
        QPointF newPos = position() + delta;
        setPosition(newPos);
        d->_previousScenePosition = scenePos;
    }

    else
    {
        QQuickItem::mouseMoveEvent(event);
    }
}
