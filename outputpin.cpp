#include "outputpin.h"
#include "pin_p.h"
#include "dataflowgraphscene.h"
#include <QDrag>
#include <QMimeData>
#include "objectmimedata.h"
#include <QPixmap>
#include <QPainter>

class OutputPinPrivate : public PinPrivate
{
public:
    QString _signalName;
    bool _clicked;
    OutputPinPrivate() : PinPrivate(), _clicked(false)
    {

    }
};
OutputPin::~OutputPin()
{

}
OutputPin::OutputPin(OutputPinPrivate &dd, QQuickItem *parent)
    : Pin(dd, parent)
{

}
OutputPin::OutputPin(QQuickItem *parent) :
    Pin(*new OutputPinPrivate(), parent)
{
    setAcceptedMouseButtons(Qt::LeftButton);
}
void OutputPin::setSignalName(QString name)
{
    Q_D(OutputPin);
    d->_signalName = name;
    emit signalNameChanged();
}
QString OutputPin::signalName() const
{
    Q_D(const OutputPin);
    return d->_signalName;
}
void OutputPin::mousePressEvent(QMouseEvent *event)
{
    Q_D(OutputPin);
    event->accept();
    d->_clicked = true;
}
void OutputPin::mouseMoveEvent(QMouseEvent *event)
{
    Q_D(OutputPin);
    if(d->_clicked)
    {
        QFont font;
        font.setPointSize(8);
        QFontMetrics metrics(font);
        QSize size(metrics.width(d->_signalName),metrics.height());
        QPixmap pixmap(size);
        pixmap.fill(Qt::transparent);
        QPainter painter(&pixmap);
        painter.setFont(font);
        painter.drawText(QRect(0,0,size.width(),size.height()),Qt::AlignCenter, d->_signalName);
        painter.end();

        QDrag *drag = new QDrag(this);
        ObjectMimeData *mimeData = new ObjectMimeData();
        mimeData->setData("application/outputpin", "...");
        mimeData->setObject(this);
        drag->setMimeData(mimeData);
        drag->setPixmap(pixmap);
        drag->exec();
        //DataflowGraphScene* sc = (DataflowGraphScene*)scene();
        //sc->onDragFinished();
    }
    Pin::mouseMoveEvent(event);
}
void OutputPin::mouseReleaseEvent(QMouseEvent *event)
{
    Q_D(OutputPin);
    d->_clicked = false;
    Pin::mouseReleaseEvent(event);
}
