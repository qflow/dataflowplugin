#ifndef INPUTPIN_H
#define INPUTPIN_H

#include <QQuickItem>
#include "pin.h"

class InputPin : public Pin
{
    Q_OBJECT
    Q_PROPERTY(QString slotName READ slotName WRITE setSlotName NOTIFY slotNameChanged)
    QString _slotName;
protected:
    virtual void dragEnterEvent(QDragEnterEvent * event);
    virtual void dragLeaveEvent(QDragLeaveEvent * event);
    virtual void dropEvent(QDropEvent * event);
public:
    explicit InputPin(QQuickItem *parent = 0);
    QString slotName();
    void setSlotName(QString name);
signals:
    void slotNameChanged();
    void connectionEstablished();
public slots:
};
QML_DECLARE_TYPE(InputPin)
#endif // INPUTPIN_H
