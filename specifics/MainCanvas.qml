import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Window 2.0
import QtQuick.Layouts 1.0
import Bauhaus 1.0

/*ScrollView
{
    DataflowGraphScene
    {
        width: 1000
        height: 1000
        id: scene
    }
}*/
Rectangle
{
    color: "black"
    DataflowGraphScene
    {
        width: 1000
        height: 1000
        id: scene
        Rectangle
        {
            anchors.fill: parent
            color: "grey"
        }
    }
}
