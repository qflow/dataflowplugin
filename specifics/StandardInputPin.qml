import QtQuick 2.1
import Bauhaus 1.0

InputPin {
    width: 18
    height: 18
    hotspot.x: triangle.x
    hotspot.y: triangle.y + triangle.height/2
    property alias orientation: triangle.orientation
    property alias color: triangle.color
    Triangle
    {
        id: triangle
        color: "#bcc8dd"
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        width: 13
        height: 13
    }
    PropertyAnimation {
        id: highlightAnimation
        target: triangle
        properties: "width,height"
        duration: 200
        easing.type: Easing.OutCubic
        from: 13
        to: 18
    }
    PropertyAnimation {
        id: unhighlightAnimation
        target: triangle
        properties: "width,height"
        duration: 200
        easing.type: Easing.OutCubic
        from: 18
        to: 13
    }
    onHoverEnter:
    {
        highlightAnimation.start();
    }
    onHoverLeave:
    {
        unhighlightAnimation.start();
    }

    /*MouseArea{
        hoverEnabled: true
        anchors.fill: parent
        onEntered:{
            highlightAnimation.start();
        }
        onExited:
        {
            unhighlightAnimation.start();
        }
    }*/
}
