import QtQuick 2.1
import Bauhaus 1.0

Bezier
{
    id: bezier
    color: "blue"
    startPoint: node.startPoint
    endPoint: node.endPoint
    z: 1
    onPressed:
    {
        node.selected = 1;
    }

    function updateControlPoints()
    {
        bezier.startControlPoint = Qt.point(node.startPoint.x + 80,
                                            node.startPoint.y);
        bezier.endControlPoint = Qt.point(node.endPoint.x - 80,
                                          node.endPoint.y);
        bezier.update();
    }

    Connections
    {
        target: node
        onEndPointChanged:
        {
            bezier.updateControlPoints();
        }
        onStartPointChanged:
        {
            bezier.updateControlPoints();
        }
        onSelectedChanged:
        {
            if(node.selected) bezier.color = "red";
            else bezier.color = "blue";
        }
    }

    Component.onCompleted:
    {
        bezier.updateControlPoints();
    }

}
