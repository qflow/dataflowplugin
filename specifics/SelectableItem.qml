import QtQuick 2.1
import Bauhaus 1.0

BasicItem
{
    onSelectedChanged:{
        node.selected = selected;
    }
    selected: node.selected
}
