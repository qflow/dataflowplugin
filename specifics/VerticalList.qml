import QtQuick 2.1
import Bauhaus 1.0

ListProperty
{
    id: listProperty
    width: column.width
    height: column.height
    z: 1
    property bool centerItems: true
    Column
    {
    id: column

        spacing: 5
        Item
        {
            width: 50
            height: 2
        }
    }

    onChildrenChanged:
    {
        for(var i=0;i<children.length;++i)
        {
            var item = children[i];
            if(item !== column)
            {
                item.x = 0;
                item.y = 0;
                item.parent = column;
                item.movable = false;
                if(listProperty.centerItems) item.anchors.horizontalCenter = column.horizontalCenter;
            }
        }
    }
}
