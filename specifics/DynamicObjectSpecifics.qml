import QtQuick 2.1
import Bauhaus 1.0

SelectableItem
{
    id: basicitem1
    width: 200
    height: 100

    onSelectedChanged:
    {
        if(selected)
            rectangle1.border.color = "red";
        else
            rectangle1.border.color = "darkCyan";
    }


            Connections
            {
                target: node
                onNodeIdChanged:
                {
                    if(node.nodeId == "")
                    {
                        var pieces = node.typeName.split('.');
                        text1.text = pieces[pieces.length - 1];
                    }
                    else
                    {
                        text1.text = node.nodeId
                    }
                }
            }
             Rectangle {
                 id: rectangle1
                 color: "#7d7c7373"
                 radius: 9
                 border.width: 3
                 border.color: "darkCyan"
                 anchors.fill: parent

                 Text
                 {
                     id: text1
                     anchors.horizontalCenter: image1.horizontalCenter
                     anchors.bottom: parent.top
                     font.pointSize: 9
                     horizontalAlignment: Text.AlignHCenter
                     Component.onCompleted:
                     {

                     }
                 }

                 Image {
                     id: image1
                     width: 32
                     height: 32
                     anchors.top: parent.top
                     anchors.topMargin: 0
                     anchors.horizontalCenter: parent.horizontalCenter
                     fillMode: Image.PreserveAspectFit
                     source: node.iconPath
                 }



                 Column {
                     id: row1
                     x: 135
                     width: 57
                     spacing: 3
                     anchors.left: parent.left
                     anchors.leftMargin: 8
                     anchors.rightMargin: 8
                     anchors.bottom: parent.bottom
                     anchors.bottomMargin: 8
                     anchors.top: parent.top
                     anchors.topMargin: 8
                 }
                 Component.onCompleted:
                 {
                     //var newObject = Qt.createQmlObject('import QtQuick 1.0; Rectangle {color: "red"; width: 20; height: 20}',
                          //rectangle1, "dynamicSnippet1");


                 }
                 Connections
                 {
                     target: node
                     onChildrenChanged:
                     {
                         for(var i=0;i<node.children.length;++i)
                         {
                             var item = node.children[i].item;
                             if(item.parent !== row1)
                             {
                                 item.x = 0;
                                 item.y = 0;
                                 item.parent = row1;
                             }
                         }
                     }
                 }



             }
}
