import QtQuick 2.1
import Bauhaus 1.0

SelectableItem
{
    width: 75
    height: 18
    movable: false
    onSelectedChanged:
    {
        if(selected)
            rectangle1.border.color = "red";
        else
            rectangle1.border.color = "darkCyan";
    }
Rectangle {
    id: rectangle1
    anchors.fill: parent
    color: "#00000000"
    border.color: "darkCyan"

    StandardInputPin
    {
        anchors.left: parent.left
        anchors.verticalCenter: parent.verticalCenter
        slotName: "in"
    }

    Text {
        id: text1
        x: 15
        width: 40
        height: 15
        text: node.propertyValue("objectName")
        font.pixelSize: 12
    }
}
}
