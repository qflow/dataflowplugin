import QtQuick 2.1
import Bauhaus 1.0

BasicItem
{
    width: 50
    height: 50
    onPressed:
    {
        node.selected = 1;
    }

    Connections
    {
        target: node
        onNodeIdChanged:
        {
            if(node.nodeId === "")
            {
                var pieces = node.typeName.split('.');
                text1.text = pieces[pieces.length - 1];
            }
            else
            {
                text1.text = node.nodeId;
            }
        }
        onSelectedChanged:
        {
            if(node.selected) innerRectangle.border.color = "red";
            else innerRectangle.border.color = "darkCyan";
        }
    }
    Rectangle {
        id: innerRectangle
        color: "#7d7c7373"
        radius: 9
        border.width: 3
        border.color: "darkCyan"
        anchors.fill: parent

        Text
        {
            id: text1
            anchors.horizontalCenter: innerRectangle.horizontalCenter
            anchors.bottom: parent.top
            font.pointSize: 9
            horizontalAlignment: Text.AlignHCenter
            Component.onCompleted:
            {

            }
        }

        Image {
            id: image1
            width: 32
            height: 32
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            fillMode: Image.PreserveAspectFit
            source: node.iconPath
        }


    }
}
