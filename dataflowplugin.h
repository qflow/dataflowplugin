#ifndef DATAFLOWPLUGIN_H
#define DATAFLOWPLUGIN_H

#include "dataflowplugin_global.h"

#include <extensionsystem/iplugin.h>

namespace Dataflow {
namespace Internal {

class DATAFLOWPLUGINSHARED_EXPORT DataflowPlugin : public ExtensionSystem::IPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QtCreatorPlugin" FILE "DataflowPlugin.json")
    
    void registerTypes();
public:
    DataflowPlugin();
    ~DataflowPlugin();
    
    bool initialize(const QStringList &arguments, QString *errorString);
    void extensionsInitialized();
    ShutdownFlag aboutToShutdown();
    
private slots:
    void triggerAction();
};

} // namespace Internal
} // namespace Dataflow

#endif // DATAFLOWPLUGIN_H

