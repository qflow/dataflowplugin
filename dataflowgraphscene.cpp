#include "dataflowgraphscene.h"
#include "basicitem.h"
#include "basicitem_p.h"
#include "pin.h"
#include "dataflowdesignerview.h"
#include "objectmimedata.h"
#include "listproperty.h"
#include <QQuickWindow>
#include <QMimeData>
#include <QPointer>
#include <nodemetainfo.h>
#include <qmlobjectnode.h>
#include <itemlibraryinfo.h>

using namespace QmlDesigner;

class DataflowGraphScenePrivate
{
public:
    QPointF _previousPosition;
    QPointer<DataflowDesignerView> _view;
    NodeInfoPointer _newNode;
    void reparentToRoot(ModelNode node);
    NodeInfoPointer createNode(ItemLibraryEntry entry);
    QList<QQuickItem*> itemsAt(QPointF pos, QQuickItem* item);
};

DataflowGraphScene::DataflowGraphScene(QQuickItem *parent) :
    QQuickItem(parent), d_ptr(new DataflowGraphScenePrivate)
{
    setFlag(QQuickItem::ItemAcceptsDrops);
    setAcceptedMouseButtons( Qt::LeftButton );
}
DataflowGraphScene::~DataflowGraphScene()
{

}
QList<QQuickItem*> DataflowGraphScenePrivate::itemsAt(QPointF pos, QQuickItem *item)
{
    QList<QQuickItem*> list;
    QQuickItem* child = item->childAt(pos.x(), pos.y());
    if(child)
    {
        list.append(child);
        QPointF coords = child->mapFromItem(item, pos);
        QList<QQuickItem*> childList = itemsAt(coords, child);
        list.append(childList);
    }
    return list;
}

QList<QQuickItem*> DataflowGraphScene::itemsAt(QPointF pos)
{
    Q_D(DataflowGraphScene);
    return d->itemsAt(pos, this);
}

void DataflowGraphScene::setView(DataflowDesignerView *value)
{
    Q_D(DataflowGraphScene);
    d->_view = value;
}

void DataflowGraphScene::mouseMoveEvent(QMouseEvent* event)
{
    Q_D(DataflowGraphScene);
    if(event->buttons() & Qt::LeftButton)
    {
        event->accept();
        QPointF scenePos = QQuickItem::window()->mapFromGlobal(event->globalPos());
        QPointF delta = scenePos - d->_previousPosition;
        QPointF newPos = position() + delta;

        setPosition(newPos);
        d->_previousPosition = scenePos;
    }

    else
    {
        QQuickItem::mouseMoveEvent(event);
    }
}
void DataflowGraphScene::addItem(QQuickItem *item)
{
    BasicItem* basicItem = qobject_cast<BasicItem*>(item);
    if(basicItem) basicItem->d_ptr->_scene = this;
    item->setParentItem(this);
}
void DataflowGraphScene::wheelEvent(QWheelEvent *event)
{
    //event->accept();fl
    QQuickItem::wheelEvent(event);
}
void DataflowGraphScene::mouseDoubleClickEvent(QMouseEvent *event)
{
    Q_D(DataflowGraphScene);
    d->_view->deselectAll();
    QQuickItem::mouseDoubleClickEvent(event);
}

void DataflowGraphScene::mousePressEvent(QMouseEvent *event)
{
    Q_D(DataflowGraphScene);
    d->_previousPosition = QQuickItem::window()->mapFromGlobal(event->globalPos());
    event->accept();
}
void DataflowGraphScene::geometryChanged(const QRectF &newGeometry, const QRectF &oldGeometry)
{
    QQuickItem::geometryChanged(newGeometry, oldGeometry);
}
void DataflowGraphScene::dragEnterEvent(QDragEnterEvent *event)
{
    if (event->mimeData()->hasFormat("application/vnd.bauhaus.itemlibraryinfo") ||
            event->mimeData()->hasFormat("application/vnd.bauhaus.libraryresource")) {
        event->accept();
    }
    if(event->mimeData()->hasFormat("application/outputpin"))
    {
        event->accept();
    }
}
void DataflowGraphScenePrivate::reparentToRoot(ModelNode node)
{
    ModelNode rootNode = _view->rootModelNode();
    if(rootNode.hasDefaultNodeAbstractProperty())
    {
        NodeAbstractProperty prop = rootNode.defaultNodeAbstractProperty();
        NodeListProperty defaultProperty = prop.toNodeListProperty();
        if(defaultProperty.isValid())
        {
            defaultProperty.reparentHere(node);
        }
    }
}
NodeInfoPointer DataflowGraphScenePrivate::createNode(ItemLibraryEntry entry)
{
    QList<QPair<PropertyName, QVariant> > propertyPairList;

    if (entry.qmlSource().isEmpty())
    {
        foreach (const PropertyContainer &property, entry.properties())
            propertyPairList.append(qMakePair(property.name(), property.value()));
    }
    ModelNode modelNode = _view->createModelNode(entry.typeName(),
                                                 entry.majorVersion(),
                                                 entry.minorVersion(),
                                                 propertyPairList);
    QmlObjectNode objNode = QmlObjectNode(modelNode);

    QString id;
    int i = 1;
    QString name(entry.name().toLower());
    name.replace(QRegExp(QLatin1String("[^a-zA-Z0-9_]")), QLatin1String("_"));
    do {
        id = name + QString::number(i);
        i++;
    } while (_view->hasId(id));

    objNode.setId(id);
    reparentToRoot(objNode.modelNode());
    NodeInfoPointer nodeInfo = _view->nodeInfo(objNode.modelNode().internalId());
    return nodeInfo;
}
void DataflowGraphScene::dragMoveEvent(QDragMoveEvent *event)
{
    Q_D(DataflowGraphScene);
    if (event->mimeData()->hasFormat("application/vnd.bauhaus.itemlibraryinfo") ||
            event->mimeData()->hasFormat("application/vnd.bauhaus.libraryresource")) {
        event->accept();


        if(!d->_newNode.isNull())
        {
            QQuickItem* item = d->_newNode->item();
            QPointF pos = event->posF();
            pos.setX(pos.x() - item->width()/2);
            pos.setY(pos.y() - item->height()/2);
            item->setPosition(pos);

            QList<QQuickItem*> items = itemsAt(event->posF());
            bool found = false;
            foreach (QQuickItem* i, items) {
                ListProperty* prop = dynamic_cast<ListProperty*>(i);
                if(prop)
                {
                    NodeListProperty nodeListProp = prop->nodeListProperty();
                    NodeMetaInfo info = prop->modelNode().metaInfo();
                    TypeName listType = info.propertyTypeName(nodeListProp.name());
                    if(nodeListProp.isValid())
                    {
                        bool isSubclass = d->_newNode->modelNode().metaInfo().
                                isSubclassOf(listType, info.majorVersion(), info.minorVersion());
                        if(isSubclass)
                        {
                            nodeListProp.reparentHere(d->_newNode->modelNode());
                            item->setParentItem(prop);
                            found = true;
                        }
                    }
                }
            }
            if(!found)
            {
                item->setParentItem(NULL);
                d->reparentToRoot(d->_newNode->modelNode());
            }
            return;
        }
        QDataStream stream(event->mimeData()->data("application/vnd.bauhaus.itemlibraryinfo"));
        ItemLibraryEntry itemLibraryEntry;
        stream >> itemLibraryEntry;
        qDebug() << itemLibraryEntry.name();
        d->_newNode = d->createNode(itemLibraryEntry);
        qDebug() << d->_newNode->nodeId();
        d->_newNode->item()->setPosition(event->posF());
        d->_newNode->item()->setZ(1);
    }


    if(event->mimeData()->hasFormat("application/outputpin"))
    {
        event->accept();
        ObjectMimeData* data = (ObjectMimeData*)event->mimeData();
        d->_view->onDraggingOutputPin((OutputPin*)data->getObject(), event);
    }
    QQuickItem::dragMoveEvent(event);
}
void DataflowGraphScene::onInputPinLeave(InputPin *pin)
{
    Q_D(DataflowGraphScene);
    d->_view->onInputPinLeave(pin);
}
void DataflowGraphScene::onConnectionEstablished(InputPin *pin)
{
    Q_D(DataflowGraphScene);
    d->_view->onConnectionEstablished(pin);
}
void DataflowGraphScene::onInputPinEnter(InputPin *inputPin)
{
    Q_D(DataflowGraphScene);
    d->_view->onInputPinEnter(inputPin);
}

