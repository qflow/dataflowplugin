TARGET = DataflowPlugin
TEMPLATE = lib

QT += quick widgets qml
DEFINES += DATAFLOWPLUGIN_LIBRARY
# DataflowPlugin files

SOURCES += \
    zoomaction.cpp \
    #module.cpp \
    nodeinfo.cpp \
    basicitem.cpp \
    triangle.cpp \
    inputpin.cpp \
    outputpin.cpp \
    pin.cpp \
    connectioninfo.cpp \
    dataflowplugin.cpp \
    objectmimedata.cpp \
    dataflowcontext.cpp \
    librarynodeinfo.cpp \
    listproperty.cpp \
    dataflowdesignerview.cpp \
    dataflowdesignerwidget.cpp \
    dataflowgraphscene.cpp \
    dataflowgraphicsview.cpp \
    bezier.cpp

HEADERS += \
    zoomaction.h \
    #module.h \
    nodeinfo.h \
    basicitem.h \
    triangle.h \
    inputpin.h \
    outputpin.h \
    pin.h \
    connectioninfo.h \
    dataflowplugin_global.h \
    dataflowpluginconstants.h \
    dataflowplugin.h \
    objectmimedata.h \
    dataflowcontext.h \
    librarynodeinfo.h \
    listproperty.h \
    dataflowdesignerview.h \
    dataflowdesignerwidget.h \
    dataflowgraphscene.h \
    dataflowgraphicsview.h \
    nodeinfo_p.h \
    basicitem_p.h \
    bezier.h \
    pin_p.h

# Qt Creator linking

## set the QTC_SOURCE environment variable to override the setting here
QTCREATOR_SOURCES = $$(QTC_SOURCE)
isEmpty(QTCREATOR_SOURCES):QTCREATOR_SOURCES=/home/michal/workspace/qt-creator

## set the QTC_BUILD environment variable to override the setting here
IDE_BUILD_TREE = $$(QTC_BUILD)
isEmpty(IDE_BUILD_TREE):IDE_BUILD_TREE=/home/michal/workspace/qt-creator

## uncomment to build plugin into user config directory
## <localappdata>/plugins/<ideversion>
##    where <localappdata> is e.g.
##    "%LOCALAPPDATA%\QtProject\qtcreator" on Windows Vista and later
##    "$XDG_DATA_HOME/data/QtProject/qtcreator" or "~/.local/share/data/QtProject/qtcreator" on Linux
##    "~/Library/Application Support/QtProject/Qt Creator" on Mac
#USE_USER_DESTDIR = yes

PROVIDER = QFlow

DEFINES += PROVIDER=\\\"$$PROVIDER\\\"

include($$QTCREATOR_SOURCES/src/qtcreatorplugin.pri)

LIBS += -L$$IDE_PLUGIN_PATH/QtProject

FORMS +=

DEFINES -= QT_NO_CAST_FROM_ASCII

OTHER_FILES += \
    specifics/StandardOutputPin.qml \
    specifics/StandardInputPin.qml \
    specifics/SelectableItem.qml \
    specifics/QtObjectSpecifics.qml \
    specifics/IDynamicSlotSpecifics.qml \
    specifics/DynamicObjectSpecifics.qml \
    specifics/DirectConnectionSpecifics.qml \
    specifics/VerticalList.qml \
    specifics/MainCanvas.qml

RESOURCES += \
    dataflowplugin.qrc


