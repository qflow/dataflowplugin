#ifndef OUTPUTPIN_H
#define OUTPUTPIN_H

#include "pin.h"

class OutputPinPrivate;
class OutputPin : public Pin
{
    Q_OBJECT
    Q_PROPERTY(QString signalName READ signalName WRITE setSignalName NOTIFY signalNameChanged)
protected:
    void mousePressEvent(QMouseEvent * event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent * event);
public:
    explicit OutputPin(QQuickItem *parent = 0);
    ~OutputPin();
    void setSignalName(QString name);
    QString signalName() const;
signals:
    void signalNameChanged();
public slots:
protected:
    OutputPin(OutputPinPrivate &dd, QQuickItem *parent);
private:
    Q_DECLARE_PRIVATE(OutputPin)
    
};

#endif // OUTPUTPIN_H
