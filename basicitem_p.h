#ifndef BASICITEM_P_H
#define BASICITEM_P_H

#include "dataflowgraphscene.h"

class BasicItemPrivate
{
public:
    bool _movable;
    bool _selected;
    QPointF _previousScenePosition;
    DataflowGraphScene* _scene;
};

#endif // BASICITEM_P_H
