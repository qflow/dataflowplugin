#include "inputpin.h"
#include "dataflowgraphscene.h"

InputPin::InputPin(QQuickItem *parent) :
    Pin(parent)
{
    setFlag(QQuickItem::ItemAcceptsDrops);
}
void InputPin::setSlotName(QString name)
{
    _slotName = name;
    emit slotNameChanged();
}
QString InputPin::slotName()
{
    return _slotName;
}
void InputPin::dragEnterEvent(QDragEnterEvent *event)
{
    event->accept();
    emit hoverEnter();
    scene()->onInputPinEnter(this);
}
void InputPin::dragLeaveEvent(QDragLeaveEvent *event)
{
    event->accept();
    scene()->onInputPinLeave(this);
    emit hoverLeave();
}
void InputPin::dropEvent(QDropEvent *event)
{
    event->accept();
    emit connectionEstablished();
    scene()->onConnectionEstablished(this);
}

