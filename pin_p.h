#ifndef PIN_P_H
#define PIN_P_H

#include "dataflowgraphscene.h"
#include <QPointer>

class DataflowGraphScene;

class PinPrivate
{
public:
    QPointF _hotspot;
    QPointer<DataflowGraphScene> _scene;
    DataflowGraphScene* findScene(QQuickItem* item);
    PinPrivate()
    {

    }
    virtual ~PinPrivate()
    {
    }
};

#endif // PIN_P_H
