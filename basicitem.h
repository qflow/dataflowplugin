#ifndef BASICITEM_H
#define BASICITEM_H

#include "dataflowgraphscene.h"
#include <modelnode.h>

using namespace QmlDesigner;

class BasicItemPrivate;
class BasicItem : public QQuickItem
{
    Q_OBJECT
    Q_PROPERTY(bool movable READ movable WRITE setMovable NOTIFY movableChanged)
    friend class DataflowGraphScene;
public:
    explicit BasicItem(QQuickItem *parent = 0);
    ~BasicItem();
    void setMovable(bool val);
    bool movable() const;
    ModelNode modelNode() const;
signals:
    void movableChanged();
    void pressed();
public slots:
protected:
    void mousePressEvent(QMouseEvent * event);
    void mouseReleaseEvent(QMouseEvent * event);
    void mouseMoveEvent(QMouseEvent * event);
    void geometryChanged(const QRectF & newGeometry, const QRectF & oldGeometry);
    const QScopedPointer<BasicItemPrivate> d_ptr;
    BasicItem(BasicItemPrivate &dd, QQuickItem *parent);
private:
    static void updatePins(QQuickItem *item);
    Q_DECLARE_PRIVATE(BasicItem)
    
};

#endif // BASICITEM_H
