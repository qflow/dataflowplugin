#ifndef DATAFLOWDESINERVIEW_H
#define DATAFLOWDESINERVIEW_H

#include "nodeinfo.h"
#include "outputpin.h"
#include "inputpin.h"
#include <qmldesigner/designercore/include/abstractview.h>
#include "dataflowplugin_global.h"

using namespace QmlDesigner;

class DataflowDesignerViewPrivate;
class DATAFLOWPLUGINSHARED_EXPORT DataflowDesignerView : public AbstractView
{
    Q_OBJECT

protected slots:
    void onSelectedChanged();
public:
    explicit DataflowDesignerView(QObject *parent = 0);
    ~DataflowDesignerView();
    bool hasWidget() const;
    WidgetInfo widgetInfo();
    QQmlEngine* engine() const;
    NodeInfoPointer nodeInfo(quint32 internalId);
    static QList<QQuickItem*> getAllDescendands(QQuickItem* item);
    void deselectAll();
    void nodeCreated(const ModelNode &createdNode) override;
    void modelAttached(Model *model) override;
    void nodeRemoved(const ModelNode &removedNode,
                     const NodeAbstractProperty &parentProperty,
                     PropertyChangeFlags propertyChange) override;
    void selectedNodesChanged(const QList<ModelNode> &selectedNodeList,
                              const QList<ModelNode> &lastSelectedNodeList) override;
    void nodeIdChanged(const ModelNode& node, const QString& newId, const QString& oldId) override;
    void nodeAboutToBeRemoved(const ModelNode &removedNode) override;
    void setCenter(bool center);
    void modelAboutToBeDetached(Model *model) override;


    void nodeAboutToBeReparented(const ModelNode &node, const NodeAbstractProperty &newPropertyParent,
                                 const NodeAbstractProperty &oldPropertyParent,
                                 AbstractView::PropertyChangeFlags propertyChange) override;
    void nodeReparented(const ModelNode &node, const NodeAbstractProperty &newPropertyParent,
                        const NodeAbstractProperty &oldPropertyParent,
                        AbstractView::PropertyChangeFlags propertyChange) override;
    void propertiesAboutToBeRemoved(const QList<AbstractProperty>& propertyList) override;
    void propertiesRemoved(const QList<AbstractProperty>& propertyList) override;
    void variantPropertiesChanged(const QList<VariantProperty>& propertyList,
                                  PropertyChangeFlags propertyChange) override;
    void bindingPropertiesChanged(const QList<BindingProperty>& propertyList,
                                  PropertyChangeFlags propertyChange) override;
    void signalHandlerPropertiesChanged(const QVector<SignalHandlerProperty>& propertyList,
                                        PropertyChangeFlags propertyChange) override;
    void rootNodeTypeChanged(const QString &type, int majorVersion, int minorVersion) override;
    void scriptFunctionsChanged(const ModelNode &node, const QStringList &scriptFunctionList) override;
    void instancePropertyChange(const QList<QPair<ModelNode, PropertyName> > &propertyList) override;
    void instancesCompleted(const QVector<ModelNode> &completedNodeList) override;
    void instanceInformationsChange(const QMultiHash<ModelNode, InformationName> &informationChangeHash) override;
    void instancesRenderImageChanged(const QVector<ModelNode> &nodeList) override;
    void instancesPreviewImageChanged(const QVector<ModelNode> &nodeList) override;
    void instancesChildrenChanged(const QVector<ModelNode> &nodeList) override;
    void instancesToken(const QString &tokenName, int tokenNumber, const QVector<ModelNode> &nodeVector) override;
    void nodeSourceChanged(const ModelNode &modelNode, const QString &newNodeSource) override;
    void rewriterBeginTransaction() override;
    void rewriterEndTransaction() override;
    void currentStateChanged(const ModelNode &node) override;
    void fileUrlChanged(const QUrl &oldUrl, const QUrl &newUrl) override;
    void nodeOrderChanged(const NodeListProperty &listProperty, const ModelNode &movedNode, int oldIndex) override;
    void auxiliaryDataChanged(const ModelNode &node, const PropertyName &name, const QVariant &data) override;
    void customNotification(const AbstractView *view, const QString &identifier,
                            const QList<ModelNode> &nodeList, const QList<QVariant> &data) override;
    void importsChanged(const QList<Import> &addedImports, const QList<Import> &removedImports) override;
signals:

public slots:
    void onDraggingOutputPinFinished();
    void onDraggingOutputPin(OutputPin* outputPin, QDragMoveEvent *event);
    void onInputPinLeave(InputPin* pin);
    void onInputPinEnter(InputPin* inputPin);
    void onConnectionEstablished(InputPin* inputPin);
private:
    const QScopedPointer<DataflowDesignerViewPrivate> d_ptr;
    Q_DECLARE_PRIVATE(DataflowDesignerView)
};

#endif // DATAFLOWDESINERVIEW_H
