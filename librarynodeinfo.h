#ifndef LIBRARYNODEINFO_H
#define LIBRARYNODEINFO_H

#include "nodeinfo.h"

class LibraryNodeInfoPrivate;
class LibraryNodeInfo : public NodeInfo
{
    Q_OBJECT
    Q_PROPERTY(QString iconPath READ iconPath WRITE setIconPath NOTIFY iconPathChanged)
public:
    explicit LibraryNodeInfo(QObject *parent = 0);
    ~LibraryNodeInfo();
    void setIconPath(QString path);
    QString iconPath();
signals:
    void iconPathChanged();
public slots:
protected:
    LibraryNodeInfo(LibraryNodeInfoPrivate &dd, QObject *parent);
private:
    Q_DECLARE_PRIVATE(LibraryNodeInfo)
};

#endif // LIBRARYNODEINFO_H
