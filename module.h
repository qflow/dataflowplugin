#ifndef MODULE_H
#define MODULE_H

#include <QGraphicsObject>
#include <modelnode.h>

using namespace QmlDesigner;

class Module : public QGraphicsObject
{
    Q_OBJECT

    ModelNode _node;
protected:
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    QPainterPath shape () const;
public:
    Module(QGraphicsItem * parent = 0);
    void setModelNode(ModelNode node);
    
signals:
    
public slots:
    
};

#endif // MODULE_H
