#include "pin.h"
#include "pin_p.h"
#include <QCursor>
#include <QPointer>

Pin::Pin(QQuickItem *parent) :
    QQuickItem(parent), d_ptr(new PinPrivate())
{
    init();
}
Pin::Pin(PinPrivate &dd, QQuickItem *parent) :
    QQuickItem(parent), d_ptr(&dd)
{
    init();
}
void Pin::init()
{
    setCursor(QCursor(Qt::ArrowCursor));
    setFlag(QQuickItem::ItemHasContents);
    setAcceptHoverEvents(true);
}

Pin::~Pin()
{

}
DataflowGraphScene* PinPrivate::findScene(QQuickItem* item)
{
    DataflowGraphScene* scene = qobject_cast<DataflowGraphScene*>(item);
    if(scene)
    {
        return scene;
    }
    QQuickItem* parent = item->parentItem();
    if(!parent) return NULL;
    return findScene(parent);
}

void Pin::itemChange(ItemChange change, const ItemChangeData & value)
{
    return QQuickItem::itemChange(change, value);
}

QPointF Pin::hotspot() const
{
    Q_D(const Pin);
    return d->_hotspot;
}
void Pin::setHotspot(QPointF point)
{
    Q_D(Pin);
    d->_hotspot = point;
    emit hotspotChanged();
    emit hotspotScenePositionChanged();
}
QPointF Pin::hotspotScenePosition() const
{
    Q_D(const Pin);
    return mapToItem(scene(), d->_hotspot);
}
QPointF Pin::scenePosition() const
{
    return mapToItem(scene(), QPointF(0, 0));
}

void Pin::hoverEnterEvent(QHoverEvent *event)
{
    QQuickItem::hoverEnterEvent(event);
    emit hoverEnter();
}
void Pin::hoverLeaveEvent(QHoverEvent *event)
{
    QQuickItem::hoverLeaveEvent(event);
    emit hoverLeave();
}
DataflowGraphScene* Pin::scene() const
{
    Q_D(const Pin);
    if(d->_scene) return d->_scene;
    else
    {
        PinPrivate* dd = const_cast<PinPrivate*>(d);
        DataflowGraphScene* scene = dd->findScene(const_cast<Pin*>(this));
        dd->_scene = scene;
    }
    return d->_scene;
}
