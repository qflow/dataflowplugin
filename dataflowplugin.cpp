#include "dataflowplugin.h"
#include "dataflowpluginconstants.h"

//#include <coreplugin/icore.h>
//#include <coreplugin/icontext.h>
#include <coreplugin/actionmanager/actionmanager.h>
#include <coreplugin/actionmanager/command.h>
#include <coreplugin/actionmanager/actioncontainer.h>
#include <coreplugin/coreconstants.h>

#include <QAction>
#include <QMessageBox>
#include <QMainWindow>
#include <QMenu>

#include <QtPlugin>
#include <QListWidget>
//#include <qmljseditor/qmljseditor.h>

//#include <extensionsystem/pluginmanager.h>

//#include <coreplugin/editormanager/ieditor.h>
//#include <projectexplorer/projectexplorer.h>
//#include <coreplugin/documentmanager.h>
//#include <coreplugin/editormanager/editormanager.h>
//#include <texteditor/itexteditor.h>

//#include <qmldesigner/designercore/include/nodemetainfo.h>
//#include <qmldesigner/designercore/include/metainfo.h>
//#include <qmldesigner/components/integration/designdocument.h>
//#include <qmljseditor/qmljseditor.h>
#include <qmldesigner/qmldesignerplugin.h>
//#include <qmljseditor/qmljseditoreditable.h>
//#include <qmljs/qmljsmodelmanagerinterface.h>
//#include <qmljstools/qmljsmodelmanager.h>

#include "dataflowdesignerview.h"
//#include <qmljs/qmljsdocument.h>

#include <qmldesigner/qmldesignerconstants.h>
#include "dataflowgraphscene.h"
#include "pin.h"
#include "basicitem.h"
#include "inputpin.h"
#include "outputpin.h"
#include "triangle.h"
#include "bezier.h"
#include "connectioninfo.h"
#include "listproperty.h"

using namespace Dataflow::Internal;
using namespace QmlDesigner;

DataflowPlugin::DataflowPlugin()
{
    // Create your members
}

DataflowPlugin::~DataflowPlugin()
{
    // Unregister objects from the plugin manager's object pool
    // Delete members
}

bool DataflowPlugin::initialize(const QStringList &arguments, QString *errorString)
{
    // Register objects in the plugin manager's object pool
    // Load settings
    // Add actions to menus
    // Connect to other plugins' signals
    // In the initialize method, a plugin can be sure that the plugins it
    // depends on have initialized their members.
    
    Q_UNUSED(arguments)
    Q_UNUSED(errorString)
    
    registerTypes();
    QAction *action = new QAction(tr("DataflowPlugin action"), this);
    Core::Command *cmd = Core::ActionManager::registerAction(action, Constants::ACTION_ID,
                                                             Core::Context(Core::Constants::C_GLOBAL));
    cmd->setDefaultKeySequence(QKeySequence(tr("Ctrl+Alt+Meta+A")));
    connect(action, SIGNAL(triggered()), this, SLOT(triggerAction()));
    
    Core::ActionContainer *menu = Core::ActionManager::createMenu(Constants::MENU_ID);
    menu->menu()->setTitle(tr("DataflowPlugin"));
    menu->addAction(cmd);
    Core::ActionManager::actionContainer(Core::Constants::M_TOOLS)->addMenu(menu);


    QmlDesignerPlugin* ints = QmlDesignerPlugin::instance();
    DataflowDesignerView* newView = new DataflowDesignerView();
    ints->viewManager().registerViewTakingOwnership(newView);
    return true;
}

void DataflowPlugin::extensionsInitialized()
{
    // Retrieve objects from the plugin manager's object pool
    // In the extensionsInitialized method, a plugin can be sure that all
    // plugins that depend on it are completely initialized.
}

ExtensionSystem::IPlugin::ShutdownFlag DataflowPlugin::aboutToShutdown()
{
    // Save settings
    // Disconnect from signals that are not needed during shutdown
    // Hide UI (if you add UI that is not in the main window directly)
    return SynchronousShutdown;
}

void DataflowPlugin::triggerAction()
{
    /*ExtensionSystem::PluginManager* pm
             = ExtensionSystem::PluginManager::instance();

    QString str =  Core::DocumentManager::currentFile();
    QmlDesignerPlugin* ints = QmlDesignerPlugin::instance();
    QList<WidgetInfo> widgetInfos = ints->viewManager().widgetInfos();
    ints->viewManager().enableWidgets();
    QmlDesigner::QmlModelView* modelView = ints->viewManager().qmlModelView();
    QmlDesigner::Model* mod = modelView->model();


    QmlDesigner::QmlItemNode itemNode = modelView->rootQmlItemNode();
    QmlDesigner::ModelNode modelNode = modelView->rootModelNode();
    TypeName typName = modelNode.type();
    QString simName = tr(modelNode.simplifiedTypeName());

    QStringList functions = modelNode.scriptFunctions();
    QList<ModelNode> subModelNodes = modelNode.allDirectSubModelNodes();
    foreach(ModelNode node, subModelNodes)
    {
        const NodeMetaInfo metaInfo = node.metaInfo();
        PropertyNameList names = metaInfo.propertyNames();
        TypeName tn = node.type();
        QString sn = tr(node.simplifiedTypeName());
        QStringList scriptFunctions = node.scriptFunctions();
        QList<AbstractProperty> properties = node.properties();

        int i=0;
    }

    QmlDesigner::TypeName typeName = itemNode.simplifiedTypeName();
    QmlDesigner::NodeMetaInfo nodeMetaInfo = mod->metaInfo(typeName);
    QmlDesigner::PropertyNameList nameList = nodeMetaInfo.propertyNames();

    QmlDesigner::QmlObjectNode rootNode = modelView->rootQmlObjectNode();
    rootNode.setId(tr("test"));
    QmlDesigner::DesignDocument* desdoc = ints->currentDesignDocument();

    QmlDesigner::ItemLibraryInfo* libraryInfo = mod->metaInfo().itemLibraryInfo();
    QList<QmlDesigner::ItemLibraryEntry> entries = mod->metaInfo().itemLibraryInfo()->entries();
    foreach(QmlDesigner::ItemLibraryEntry entry, entries)
    {
        QString name = entry.name();
        QString typeName = tr(entry.typeName());
        QString category = entry.category();


    }

    mod->nodeInstanceView()->nodeIdChanged(rootNode, str,rootNode.id());

    Core::IEditor* editor = Core::EditorManager::currentEditor();

    QmlJS::ModelManagerInterface* inter = QmlJS::ModelManagerInterface::instance();

    QmlJSTools::Internal::ModelManager* modelManager = (QmlJSTools::Internal::ModelManager*)inter;
    TextEditor::BaseTextEditor* textEditor = (TextEditor::BaseTextEditor*)editor;
    QmlJSEditor::QmlJSTextEditorWidget* widget = (QmlJSEditor::QmlJSTextEditorWidget*)
            textEditor->editorWidget();
    QmlJSTools::SemanticInfo info = widget->semanticInfo();
    QStringList list = modelManager->importPaths();


    QmlJS::Snapshot s = inter->snapshot();

    const QmlJS::Context* con = info.context.data();
    const QmlJS::Document* doc = info.document.data();
    AST::Node* root = doc->ast();
    AST::Node::Kind kind = (AST::Node::Kind)root->kind;
    const QmlJS::Imports* imports = con->imports(doc);
    ScopeChain scopeChain = info.scopeChain();
    QList<const ObjectValue*> objectValues = scopeChain.qmlScopeObjects();
    foreach(const ObjectValue* value, objectValues)
    {
        QString className = value->className();
    }


    foreach(QmlJS::Import import, imports->all())
    {
        QmlJS::LibraryInfo libraryInfo = s.libraryInfo(import.libraryPath);
        QList<QmlDirParser::TypeInfo> typeInfos = libraryInfo.typeInfos();
        QList<LanguageUtils::FakeMetaObject::ConstPtr> objList = libraryInfo.metaObjects();
        foreach(LanguageUtils::FakeMetaObject::ConstPtr ptr, objList)
        {
             const LanguageUtils::FakeMetaObject* metaObj = ptr.data();
             QString className = metaObj->className();
             if(className == tr("Rectangle"))
             {
                 int i=0;
             }
        }
    }



    QList<QObject*> objects = pm->allObjects();
    QListWidget* listWidget = new QListWidget;
    foreach(QObject* obj, objects)
    {
        listWidget->addItem(QString::fromUtf8(obj->metaObject()->className()));
    }
    QMessageBox::information(Core::ICore::mainWindow(),
                             tr("Action triggered"),
                             tr("This is an action from DataflowPlugin."));
    listWidget->show();*/
}

void DataflowPlugin::registerTypes()
{
    qmlRegisterType<DataflowGraphScene>("Bauhaus", 1, 0, "DataflowGraphScene");
    qmlRegisterType<Pin>("Bauhaus",1,0,"Pin");
    qmlRegisterType<NodeInfo>("Bauhaus",1,0,"NodeInfo");
    qmlRegisterType<BasicItem>("Bauhaus",1,0,"BasicItem");
    qmlRegisterType<InputPin>("Bauhaus",1,0,"InputPin");
    qmlRegisterType<OutputPin>("Bauhaus",1,0,"OutputPin");
    qmlRegisterType<Triangle>("Bauhaus",1,0,"Triangle");

    qmlRegisterType<Bezier>("Bauhaus",1,0,"Bezier");
    qmlRegisterType<ConnectionInfo>("Bauhaus",1,0,"ConnectionInfo");
    qmlRegisterType<Pin>("Bauhaus",1,0,"Pin");
    qmlRegisterType<ListProperty>("Bauhaus",1,0,"ListProperty");
}

