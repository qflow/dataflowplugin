#include "connectioninfo.h"
#include "nodeinfo_p.h"

class ConnectionInfoPrivate : public NodeInfoPrivate
{
public:
    InputPin* _slotPin;
    OutputPin* _signalPin;
    QQuickItem* _item;
    QPointF _endPoint;
    QPointF _startPoint;
    ConnectionInfoPrivate() : NodeInfoPrivate(), _slotPin(NULL)
    {

    }
};

ConnectionInfo::ConnectionInfo(QObject *parent)
    : NodeInfo(*new ConnectionInfoPrivate(), parent)
{
}
ConnectionInfo::ConnectionInfo(ConnectionInfoPrivate &dd, QObject *parent)
    : NodeInfo(dd, parent)
{

}
ConnectionInfo::~ConnectionInfo()
{

}

void ConnectionInfo::setSlotPin(InputPin *inputPin)
{
    Q_D(ConnectionInfo);
    if(!inputPin && d->_slotPin)
    {
        QObject::disconnect(d->_slotPin, SIGNAL(hotspotScenePositionChanged()), this, SLOT(onOutPinHotspotChanged()));
    }
    d->_slotPin = inputPin;
    if(d->_slotPin)
    {
        QObject::connect(d->_slotPin, SIGNAL(hotspotScenePositionChanged()), this, SLOT(onOutPinHotspotChanged()));
        d->_endPoint = d->_slotPin->hotspotScenePosition();
        emit endPointChanged();
    }
    emit slotPinChanged();
}
InputPin* ConnectionInfo::slotPin() const
{
    Q_D(const ConnectionInfo);
    return d->_slotPin;
}
void ConnectionInfo::setSignalPin(OutputPin *outputPin)
{
    Q_D(ConnectionInfo);
    if(!outputPin && d->_signalPin)
    {
        QObject::disconnect(d->_signalPin, SIGNAL(hotspotScenePositionChanged()), this, SLOT(onInPinHotspotChanged()));
    }
    d->_signalPin = outputPin;
    if(d->_signalPin)
    {
        QObject::connect(d->_signalPin, SIGNAL(hotspotScenePositionChanged()), this, SLOT(onInPinHotspotChanged()));
        d->_startPoint = d->_signalPin->hotspotScenePosition();
        emit startPointChanged();
    }
    emit signalPinChanged();
}
OutputPin* ConnectionInfo::signalPin() const
{
    Q_D(const ConnectionInfo);
    return d->_signalPin;
}
void ConnectionInfo::onOutPinHotspotChanged()
{
    Q_D(ConnectionInfo);
    if(d->_slotPin)
    {
        d->_endPoint = d->_slotPin->hotspotScenePosition();
        emit endPointChanged();
    }
}
void ConnectionInfo::onInPinHotspotChanged()
{
    Q_D(ConnectionInfo);
    if(d->_signalPin)
    {
        d->_startPoint = d->_signalPin->hotspotScenePosition();
        emit startPointChanged();
    }
}
QPointF ConnectionInfo::endPoint() const
{
    Q_D(const ConnectionInfo);
    return d->_endPoint;
}
void ConnectionInfo::setEndPoint(QPointF point)
{
    Q_D(ConnectionInfo);
    d->_endPoint = point;
    emit endPointChanged();
}
QPointF ConnectionInfo::startPoint() const
{
    Q_D(const ConnectionInfo);
    return d->_startPoint;
}
void ConnectionInfo::setStartPoint(QPointF point)
{
    Q_D(ConnectionInfo);
    d->_startPoint = point;
    emit startPointChanged();
}
