#include "librarynodeinfo.h"
#include "nodeinfo_p.h"
#include <itemlibraryinfo.h>
#include "dataflowdesignerview.h"
#include <metainfo.h>

class LibraryNodeInfoPrivate : public NodeInfoPrivate
{
public:
    QString _iconPath;
    LibraryNodeInfoPrivate() : NodeInfoPrivate()
    {

    }
};

LibraryNodeInfo::LibraryNodeInfo(QObject *parent)
    : NodeInfo(*new LibraryNodeInfoPrivate(), parent)
{
}
LibraryNodeInfo::LibraryNodeInfo(LibraryNodeInfoPrivate &dd, QObject *parent)
    : NodeInfo(dd, parent)
{

}
LibraryNodeInfo::~LibraryNodeInfo()
{

}

void LibraryNodeInfo::setIconPath(QString path)
{
    Q_D(LibraryNodeInfo);
    d->_iconPath = path;
    emit iconPathChanged();
}
QString LibraryNodeInfo::iconPath()
{
    Q_D(LibraryNodeInfo);
    ItemLibraryInfo* libraryInfo = d->_view->model()->metaInfo().itemLibraryInfo();
    ModelNode node = this->modelNode();
    QList<ItemLibraryEntry> entries = libraryInfo->entriesForType(node.type(),
                                                                  node.majorVersion(),
                                                                  node.minorVersion());
    QString iconPath;
    if(!entries.isEmpty())
    {
        ItemLibraryEntry entry = entries.first();
        iconPath = entry.libraryEntryIconPath();
    }
    iconPath = "qrc" + iconPath;
    return iconPath;
}
