#include "module.h"
#include <QPainter>
#include <QStyleOptionGraphicsItem>

Module::Module(QGraphicsItem * parent) :
    QGraphicsObject(parent)
{
    setFlag(QGraphicsItem::ItemIsMovable);
    setFlag(ItemSendsGeometryChanges);
    setFlag(QGraphicsItem::ItemIsSelectable);
}
void Module::setModelNode(ModelNode node)
{
    _node = node;
}

void Module::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(widget);
    painter->setPen(Qt::blue);
    QColor color = Qt::lightGray;
    color.setAlpha(80);
    painter->setBrush(QBrush(color));
    if(option->state & QStyle::State_Selected)
    {
        painter->setPen(QPen(Qt::DotLine));
    }
    QRectF rect = childrenBoundingRect();
    painter->drawRoundedRect(rect, 4, 2);
    if(ModelNode::isValidId(_node.id()))
    {
        painter->drawText(rect,Qt::AlignTop | Qt::AlignHCenter, _node.id());
    }
}
QPainterPath Module::shape() const
{
    QPainterPath path;
    path.addRoundedRect(boundingRect(), 4, 2);
    return path;
}
