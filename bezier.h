#ifndef BEZIER_H
#define BEZIER_H

#include <QQuickItem>

class BezierPrivate;
class Bezier : public QQuickItem
{
    Q_OBJECT
    Q_PROPERTY(QColor color READ color WRITE setColor NOTIFY colorChanged)
    Q_PROPERTY(QPointF startPoint READ startPoint WRITE setStartPoint NOTIFY startPointChanged)
    Q_PROPERTY(QPointF endPoint READ endPoint WRITE setEndPoint NOTIFY endPointChanged)
    Q_PROPERTY(QPointF startControlPoint READ startControlPoint WRITE setStartControlPoint NOTIFY startControlPointChanged)
    Q_PROPERTY(QPointF endControlPoint READ endControlPoint WRITE setEndControlPoint NOTIFY endControlPointChanged)
protected:
    QSGNode* updatePaintNode(QSGNode *oldNode, UpdatePaintNodeData *data);
    void mousePressEvent(QMouseEvent * event);
public:
    explicit Bezier(QQuickItem *parent = 0);
    ~Bezier();
    bool contains(const QPointF & point) const;
    QColor color();
    void setColor(QColor color);
    QPointF startPoint();
    void setStartPoint(QPointF startPoint);
    QPointF endPoint();
    void setEndPoint(QPointF endPoint);
    void setStartControlPoint(QPointF point);
    QPointF startControlPoint();
    void setEndControlPoint(QPointF point);
    QPointF endControlPoint();
signals:
    void colorChanged();
    void startPointChanged();
    void endPointChanged();
    void startControlPointChanged();
    void endControlPointChanged();
    void pressed();
public slots:
private:
    const QScopedPointer<BezierPrivate> d_ptr;
    Q_DECLARE_PRIVATE(Bezier)
};

#endif // BEZIER_H
